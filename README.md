# K-fiber analysis


## Installation and requirements

Create a python3 conda environment and activate it

```bash
conda create -n kfibers python=3.7
conda activate kfibers
```

Download the repository using git and cd into it

```bash
git clone git@bitbucket.org:cmartiga/kfibers.git
```

Install using setuptools. This should include all the python libraries required to run all the scripts
```bash
cd kfibers
python setup.py install
```

R dependencies include [lme4](https://cran.r-project.org/web/packages/lme4/index.html) and need to be installed before

```R
install.packages('lme4')
```


# Data

All the collected data that was analyzed in this repository can be found in the [data](https://bitbucket.org/cmartiga/kfibers/src/master/data/) directory

# Analysis

All the scripts used for statistical analysis are located in [kfibers/analysis](https://bitbucket.org/cmartiga/kfibers/src/master/kfibers/analysis/)

# Plots and figures

Scripts to generate the panels in the paper's figures derived from this analysis are in [kfibers/plots](https://bitbucket.org/cmartiga/kfibers/src/master/kfibers/plots/) and the generatede figures in [plots](https://bitbucket.org/cmartiga/kfibers/src/master/plots/)

# Citation

MCRS1 modulates the heterogeneity of microtubule minus- end morphologies in mitotic spindles.

Alejandra Laguillo-Diego, Robert Kiewisz, Carlos Martí-Gómez, Daniel Baum, Thomas Müller-Reichert, Isabelle Vernos.

bioRxiv 2022.06.03.494715; doi: [https://doi.org/10.1101/2022.06.03.494715](https://doi.org/10.1101/2022.06.03.494715)
