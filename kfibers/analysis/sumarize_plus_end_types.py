from os.path import join

import numpy as np
import pandas as pd

from kfibers.settings import RESULTS_DIR, DATASETS, WT, CONTROL, SIMCRS1
from kfibers.utils import load_model_fit, calc_open_perc_posterior


def calc_variable_summary(x):
    return({'mean': x.mean(), '95CI_lower': np.percentile(x, 2.5),
            '95CI_upper': np.percentile(x, 97.5)})


def calc_summary(traces):
    alpha = traces['alpha']
    if 'beta1' in traces:
        beta1 = traces['beta1']
    else:
        beta1 = traces['beta']
    
    params = {'alpha': alpha, 'beta1': beta1}
    if 'beta2' in traces:
        params['beta2'] = traces['beta2']
    percs = calc_open_perc_posterior(params)
    
    summary = {'% Open {}'.format(CONTROL): calc_variable_summary(percs[CONTROL]),
               '% Open {}'.format(SIMCRS1): calc_variable_summary(percs[SIMCRS1])}
    if WT in percs:
        summary['% Open WT'] = calc_variable_summary(percs[WT])
    
    if 'beta2' in params:
        summary['logOR {}'.format(CONTROL)] = calc_variable_summary(params['beta1'])
        summary['logOR {}'.format(SIMCRS1)] = calc_variable_summary(params['beta2'])
    else:
        summary['logOR {}'.format(SIMCRS1)] = calc_variable_summary(params['beta1'])
    
    summary = pd.DataFrame(summary)
    return(summary.transpose())


if __name__ == '__main__':
    for dataset in DATASETS:
        traces = load_model_fit(dataset, prefix='plus_ends')
        summary = calc_summary(traces)
        fpath = join(RESULTS_DIR, 'plus_ends.{}.fit.tsv'.format(dataset))
        summary.to_csv(fpath, sep='\t')
