from os.path import join

import numpy as np
import pandas as pd

from kfibers.settings import RESULTS_DIR, DATASETS, WT, CONTROL, SIMCRS1
from kfibers.utils import load_model_fit, get_pred_X, calc_open_perc_posterior


def calc_variable_summary(x):
    return({'mean': x.mean(), '95CI_lower': np.percentile(x, 2.5),
            '95CI_upper': np.percentile(x, 97.5)})


def calc_summary_cols(traces, cols):
    alpha = traces['alpha'][:, cols].mean(1)
    beta1 = traces['beta1'][:, cols].mean(1)
    
    params = {'alpha': alpha, 'beta1': beta1}
    if 'beta2' in traces:
        params['beta2'] = traces['beta2'][:, cols].mean(1)
    percs = calc_open_perc_posterior(params)
    
    summary = {'% Open {}'.format(CONTROL): calc_variable_summary(percs[CONTROL]),
               '% Open {}'.format(SIMCRS1): calc_variable_summary(percs[SIMCRS1])}
    if WT in percs:
        summary['% Open WT'] = calc_variable_summary(percs[WT])
    
    if 'beta2' in params:
        summary['log2(OR) {}'.format(CONTROL)] = calc_variable_summary(params['beta1']/np.log(2))
        summary['OR {}'.format(CONTROL)] = calc_variable_summary(np.exp(params['beta1']))
        summary['log2(OR) {}'.format(SIMCRS1)] = calc_variable_summary(params['beta2']/np.log(2))
        summary['OR {}'.format(SIMCRS1)] = calc_variable_summary(np.exp(params['beta2']))
        summary['log2(OR) {}vs{}'.format(SIMCRS1, WT)] = calc_variable_summary((params['beta2']+params['beta1'])/np.log(2))
        summary['OR {}vs{}'.format(SIMCRS1, WT)] = calc_variable_summary(np.exp(params['beta2']+params['beta1']))
    else:
        summary['log2(OR) {}'.format(SIMCRS1)] = calc_variable_summary(params['beta1']/np.log(2))
        summary['OR {}'.format(SIMCRS1)] = calc_variable_summary(np.exp(params['beta1']))
    
    summary = pd.DataFrame(summary)
    return(summary.transpose())
    

def calc_summary(traces, pred_X, X_threshold=0.2):
    cols0 = [np.argmin(np.abs(pred_X))]
    cols1 = pred_X <= X_threshold
    cols2 = pred_X > X_threshold
    
    s0 = calc_summary_cols(traces, cols0)
    s0['Distance'] = 'D = 0'
    s1 = calc_summary_cols(traces, cols1)
    s1['Distance'] = 'D < {}'.format(X_threshold)
    s2 = calc_summary_cols(traces, cols2)
    s2['Distance'] = 'D > {}'.format(X_threshold)
    return(pd.concat([s0, s1, s2]))


if __name__ == '__main__':
    dataset = 'full'
    traces = load_model_fit(dataset)
    pred_X = get_pred_X(dataset)
    summary = calc_summary(traces, pred_X, X_threshold=0.2)
     
    fpath = join(RESULTS_DIR, 'minus_ends.{}.fit.tsv'.format(dataset))
    summary.to_csv(fpath, sep='\t')
    
    traces = load_model_fit(dataset, prefix='interpolar.minus_ends')
    pred_X = get_pred_X(dataset, interpolar=True)
    summary = calc_summary(traces, pred_X, X_threshold=0.2)
    
    fpath = join(RESULTS_DIR, 'interpolar.minus_ends.{}.fit.tsv'.format(dataset))
    summary.to_csv(fpath, sep='\t')
    