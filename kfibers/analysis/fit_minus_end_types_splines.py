from os.path import join

import numpy as np

from kfibers.settings import TYPES, FIT_DIR, SIMCRS1, CONTROL, WT
from kfibers.utils import (get_model, get_posterior_df, get_summary_df,
                           load_minus_ends_data)


def to_stan_data(data):
    open_ends = (data[TYPES] == 1).astype(int)
    knots = np.linspace(data['start'].min(), data['end'].max(), 11)
    pred_X = np.linspace(np.min(data['start']), np.max(data['end']), 101)
    groups = data['Group'].unique()
    cols = [CONTROL, SIMCRS1]
    if WT not in groups:
        cols = [SIMCRS1]
    design = data[cols]
    stan_data = {'open_ends': open_ends, 'num_measures': open_ends.shape[1],
                 'design': design.astype(int),
                 'num_data': data.shape[0],
                 'X': data['Relative_position'], 'spline_degree': 3,
                 'num_knots': knots.shape[0], 'knots': knots,
                 'num_pred': 101, 'pred_X': pred_X,
                 'num_groups': design.shape[1]}
    return(stan_data)


def fit_model(stan_data, dataset, only_matching=False):
    params = ['pred_alpha', 'pred_beta']    
    m = get_model('splines', recompile=False)
    fit = m.sampling(data=stan_data, pars=params, control={'adapt_delta': 0.9})
    df = get_posterior_df(fit, params)[0]
    summary = get_summary_df(fit)

    if only_matching:
        dataset = '{}.only_matching'.format(dataset)

    df.to_csv(join(FIT_DIR, 'minus_ends.{}.traces.csv'.format(dataset)))
    summary.to_csv(join(FIT_DIR, 'minus_ends.{}.summary.csv'.format(dataset)))


if __name__ == '__main__':
    data = load_minus_ends_data(dataset=None, bins=25,
                                quantiles=False,
                                only_matching=False)
    stan_data = to_stan_data(data)
    fit_model(stan_data, dataset='full', only_matching=False)
