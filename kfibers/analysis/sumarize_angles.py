from os.path import join

import pandas as pd

from kfibers.settings import RESULTS_DIR, DATA_DIR


if __name__ == '__main__':
    file_dict = {'Microtubule_angle.csv': ('Angle', ['Genotype', 'Experiment']),
                 'Microtubule_Angle_EM.csv': ('Microtubule Angle', 'Condition'),
                 'Half_Spindle_Angle.csv': ('Microtubule Angle', ['Genotype', 'Experiment']),
                 'Half_Spindle_Angle_EM.csv': ('Half Spindle Angle', 'Condition')}
    
    for fname, (variable, key_group) in file_dict.items():
        fpath = join(DATA_DIR, fname)
        data = pd.read_csv(fpath)
        s = data.groupby(key_group).agg({variable:['mean','std']}).reset_index()
        s.to_csv(join(RESULTS_DIR, fname.replace('.csv', '') + '.summary.csv'))
