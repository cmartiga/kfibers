from os.path import join

import pandas as pd
import numpy as np
import statsmodels.api as sm
import statsmodels.formula.api as smf

from kfibers.settings import (SIMCRS1, CONTROL, CELL, FIBER, TYPE1, DATASETS,
                              RESULTS_DIR)
from kfibers.utils import load_minus_ends_data


def count_MT_per_fiber(data):
    data['ID'] = ['{}.{}'.format(c, f) for c, f in zip(data[CELL], data[FIBER])]
    c = data.groupby([CELL, 'ID', CONTROL, SIMCRS1])[TYPE1].count().reset_index()
    c.columns = [CELL, FIBER, CONTROL, SIMCRS1, 'n']
    return(c)


def analyze_data(counts):
    formula = 'n ~ {} + {}'.format(CONTROL, SIMCRS1)
    m = smf.glm(formula=formula, data=counts,
                family=sm.families.Poisson()).fit()
    result = pd.DataFrame({'Coefficient': m.params, 'p-value': m.pvalues,
                           'Average microtubules per fiber': [np.exp(sum(m.params[:i+1]))
                                      for i in range(3)]},
                          index=m.params.index)
    return(result)


if __name__ == '__main__':
    for dataset in DATASETS:
        data = load_minus_ends_data(dataset=dataset, rm_undefined=False)
        counts = count_MT_per_fiber(data)
        result = analyze_data(counts)
        fpath = join(RESULTS_DIR, 'MT_per_fiber.analysis.{}.tsv'.format(dataset))
        result.to_csv(fpath, sep='\t')
