from os.path import join

import pandas as pd
import numpy as np

from kfibers.settings import (TYPES, FIT_DIR, SIMCRS1, CONTROL, POSITION,
                              FIBER, CELL, DATA_DIR, GROUP)
from kfibers.utils import (get_model, get_posterior_df, get_summary_df,
                           load_minus_ends_data)


def get_var_idx(x):
    unique = x.unique().tolist()    
    return(np.array([unique.index(y) for y in x]) + 1)


def to_stan_data(data):
    data['mt'] = np.arange(data.shape[0]) + 1
    data[FIBER] = get_var_idx(data[FIBER])
    
    random = [FIBER, CELL, 'mt']
    fixed = [CONTROL, SIMCRS1, 'kmts', 'kmts_siMCRS1', 'kmts_siScramble']  
    cols = random + fixed + TYPES
    data = pd.melt(data[cols], id_vars=cols[:-2])
    data = data.loc[data['value'] != 3, :]
    data['variable'] = get_var_idx(data['variable'])
    
    data.to_csv(join(DATA_DIR, 'minus_ends_processed.csv'))
    summary = data.groupby(['siScramble', 'siMCRS1', 'kmts'])['value'].count().reset_index()
    summary.to_csv(join(DATA_DIR, 'minus_ends_processed.summary.csv'), index=False)
    
    random += ['variable']
  
    stan_data = {'N': data.shape[0],
                 'K': len(fixed),

                 'C': data[CELL].max(),
                 'F': data[FIBER].max(),
                 'T': data['mt'].max(),
                 'M': data['variable'].max(),
                 
                 'cells': data[CELL],
                 'fibers': data[FIBER],
                 'mts': data['mt'],
                 'measures': data['variable'],
                 
                 'open': (data['value'] == 1).astype(int),
                 'X': data[fixed]
                 }
    return(stan_data)


def fit_model(stan_data, dataset, only_matching=False):
    params = ['alpha', 'beta',
              'cells_sigma', 'mts_sigma', 'measure_sigma'] # , 'fibers_sigma'    
    m = get_model('end_type', recompile=False)
    fit = m.sampling(data=stan_data, pars=params, control={'adapt_delta': 0.9})
    df = get_posterior_df(fit, params)[0]
    summary = get_summary_df(fit)

    if only_matching:
        dataset = '{}.only_matching'.format(dataset)

    df.to_csv(join(FIT_DIR, 'minus_ends_interaction.{}.traces.csv'.format(dataset)))
    summary.to_csv(join(FIT_DIR, 'minus_ends_interaction.{}.summary.csv'.format(dataset)))


if __name__ == '__main__':
    rm_undefined = False
    for only_matching in [False]:
        for dataset in ['full']:
            
            # Load kmts data
            kmts = load_minus_ends_data(dataset=dataset, 
                                        only_matching=only_matching,
                                        interpolar=False,
                                        rm_undefined=rm_undefined)
            kmts['kmts'] = 1
            kmts = kmts.loc[kmts[POSITION] <= 0.2, :]
            
            # Load interpolar mts data
            interpolar = load_minus_ends_data(dataset=dataset, 
                                              only_matching=only_matching,
                                              interpolar=True,
                                              rm_undefined=rm_undefined)
            interpolar['kmts'] = 0
            sel_rows = np.logical_or(interpolar['Pole_association'] == 'Pole1',
                                     interpolar[CELL] <= 2)
            sel_rows = np.logical_and(sel_rows, interpolar[POSITION] <= 0.2)
            interpolar = interpolar.loc[sel_rows, :] 
            interpolar['Cell'] = kmts['Cell'].max() + interpolar['Cell']
            interpolar['Fiber'] = np.arange(interpolar.shape[0])

            # Merge and generate interaction terms
            data = pd.concat([kmts, interpolar])
            data['kmts_siMCRS1'] = data['kmts'] * data['siMCRS1']
            data['kmts_siScramble'] = data['kmts'] * data['siScramble']
            
            # Fit model
            stan_data = to_stan_data(data)
            fit_model(stan_data, dataset, only_matching=only_matching)
