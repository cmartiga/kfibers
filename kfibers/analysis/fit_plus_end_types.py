from os.path import join

from kfibers.settings import TYPES, DATASETS, FIT_DIR, SIMCRS1, CONTROL, WT
from kfibers.utils import (get_model, get_posterior_df, get_summary_df,
                           load_plus_ends_data)


def to_stan_data(data):
    open_ends = (data[TYPES] == 1).astype(int)
    groups = data['Group'].unique()
    cols = [CONTROL, SIMCRS1]
    if WT not in groups:
        cols = [SIMCRS1]
    design = data[cols]
    stan_data = {'open_ends': open_ends, 'num_measures': open_ends.shape[1],
                 'design': design.astype(int),
                 'num_data': data.shape[0],
                 'num_groups': design.shape[1]}
    return(stan_data)


def fit_model(stan_data, dataset, only_matching=False):
    params = ['alpha', 'beta', 'sigma']    
    m = get_model('end_type', recompile=False)
    fit = m.sampling(data=stan_data, pars=params, control={'adapt_delta': 0.9})
    df = get_posterior_df(fit, params)[0]
    summary = get_summary_df(fit)

    if only_matching:
        dataset = '{}.only_matching'.format(dataset)

    df.to_csv(join(FIT_DIR, 'plus_ends.{}.traces.csv'.format(dataset)))
    summary.to_csv(join(FIT_DIR, 'plus_ends.{}.summary.csv'.format(dataset)))


if __name__ == '__main__':
    for only_matching in [False, True]:
        for dataset in DATASETS:
            data = load_plus_ends_data(dataset=dataset, 
                                       only_matching=only_matching)
            stan_data = to_stan_data(data)
            fit_model(stan_data, dataset, only_matching=only_matching)
