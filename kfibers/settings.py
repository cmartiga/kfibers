from os.path import join, abspath, dirname

VERSION="0.1.0"

# Directories
BASE_DIR = abspath(join(dirname(__file__), '..'))
DATA_DIR = join(BASE_DIR, 'data')
RESULTS_DIR = join(BASE_DIR, 'results')
FIT_DIR = join(RESULTS_DIR, 'fit')
PLOTS_DIR = join(BASE_DIR, 'plots')
EXT_PLOTS_DIR = join(PLOTS_DIR, 'external')
MODELS_DIR = join(BASE_DIR, 'models')
COMPILED_DIR = join(MODELS_DIR, 'compiled')
STAN_CODE_DIR = join(MODELS_DIR, 'stan_code')

MODELS_LABELS = {'splines': 'end_type_splines2'}

# File paths
MINUS_ENDS_FPATH = join(DATA_DIR, 'minus_ends.csv')
PLUS_ENDS_FPATH = join(DATA_DIR, 'plus_ends.csv')
CELLS_FPATH = join(DATA_DIR, 'cells.csv')

INT_MINUS_ENDS_FPATH = join(DATA_DIR, 'interpolar.minus_ends.csv')
INT_PLUS_ENDS_FPATH = join(DATA_DIR, 'interpolar.plus_ends.csv')
INT_CELLS_FPATH = join(DATA_DIR, 'interpolar.cells.csv')


# Global variables
POSITION = 'Relative_position'
GROUP = 'Group'
CELL = 'Cell'
FIBER = 'Fiber'
WT = 'Control'
CONTROL = 'siScramble'
SIMCRS1 = 'siMCRS1'
GROUPS = [WT, CONTROL, SIMCRS1]
TYPE1 = 'EndType_1'
TYPE2 = 'EndType_2'
TYPES = [TYPE1, TYPE2]
DATASETS = ['3Cells', 'no{}'.format(WT), 'full', '1Control1c4siMCRS1']

PLOTS_FORMAT = 'png'

# In vitro params
MCRS1 = 'MCRS1'
KANSL3 = 'KANSL3'
PROTEINS = [MCRS1, KANSL3]
PROTEIN_CONC = {MCRS1: [200, 320, 400],
                KANSL3: [39.2, 78, 156]}
