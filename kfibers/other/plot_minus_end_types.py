from os.path import join

import numpy as np
import pandas as pd
import seaborn as sns

from kfibers.settings import DATA_DIR, PLOTS_DIR
from kfibers.plot_utils import init_fig, arrange_plot, savefig, PALETTE,\
    create_patches_legend


def invlogit(X):
    return(np.exp(X) / (1 + np.exp(X)))


if __name__ == '__main__':
    
    palette = PALETTE
    
    fpath = join(DATA_DIR, 'minus_end_types.single.traces.csv')
    data = pd.read_csv(fpath, index_col=0)
    p_open = pd.melt(pd.DataFrame({'WT': invlogit(data['alpha']),
                                   'siControl': invlogit(data['alpha'] + data['beta1']),
                                   'siMCRS1': invlogit(data['alpha'] + data['beta1'] + data['beta2'])})* 100)
    deltas = pd.DataFrame({'siControl': data['beta1'],
                           'siMCRS1': data['beta2']})
    p_delta = (deltas > 0).mean().to_dict()
    print(p_delta)
    deltas = pd.melt(deltas)
    
    # Fig
    fig, subplots = init_fig(1, 2, rowsize=2.7, colsize=3)
    axes = subplots[0]
    sns.violinplot(x='variable', y='value', hue='variable', data=p_open,
                    palette=palette,
                   inner='quartile', ax=axes, dodge=False)
    arrange_plot(axes, ylims=(0, 105), xlabel='Group',
                 ylabel='% minus-end open', showlegend=False)
    create_patches_legend(axes, PALETTE, loc=(-0.1, 1.1), ncol=3,
                          fontsize=8)
    
    axes = subplots[1]
    sns.violinplot(x='variable', y='value', hue='variable', data=deltas, palette=palette,
                   inner='quartile', ax=axes, dodge=False)
    axes.text(0.4,1.5,#3,
              r'$p\left(\Delta log\left(\frac{p_{open}}{p_{closed}}\right) > 0|data\right)$' + ' = {:.2f}'.format(p_delta['siMCRS1']),
              fontsize=7)
    arrange_plot(axes, xlabel='Group',
                 ylabel=r'$\Delta log\left(\frac{p_{open}}{p_{closed}}\right)$', showlegend=False,
                 hline=0)
    axes.legend(loc=(0.05, 1.1), frameon=True, fancybox=True, fontsize=8,
                ncol=2)
    
    # Savefig
    fpath = join(PLOTS_DIR, 'minus_end_types.png')
    savefig(fig, fpath)
