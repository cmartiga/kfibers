from os.path import join

import numpy as np
import pandas as pd
import seaborn as sns

from kfibers.settings import DATA_DIR, PLOTS_DIR
from kfibers.plot_utils import init_fig, savefig
from AS_quant.plot_utils import arrange_plot
from _functools import partial
from scipy.optimize.minpack import fsolve


def equil(xs, ks, n_layers=3):
    xs = np.exp(xs).reshape((n_layers, int(xs.shape[0] / n_layers)))
    a = xs[:,0]
    m = xs[:,1]
    am = xs[:,2]
    alpha, omega, deg, pol, ka, kd, delta, gamma, k0, d0 = ks
    fs = []
    
    # First level
    f1 = ka * a[0] * m[0] + pol * am[1] - kd * am[0]
    f2 = ka * a[0] * m[0] + deg * m[0] - kd * am[0] - pol * m[1]
    f3 = ka * a[0] * m[0] + delta * a[0] + d0 * a[0]  - gamma * a[1] - kd * am[0] - k0
    fs.extend([f1, f2, f3])
    
    # Inner levels
    for i in range(1, n_layers - 1):
        fx4 = pol * m[i] + ka * m[i] * a[i] + deg * m[i] - deg * m[i-1] - kd * am[i] - pol * m[i+1]
        fx5 = a[i]* (gamma + ka*m[i] + delta + d0) - delta*a[i-1] - kd * am[i] - k0 - gamma * a[i+1]
        fx6 = am[i] * (kd + pol) - ka * a[i] * m[i] - pol * am[i+1]
        fs.extend([fx4, fx5, fx6])
    
    # Last level
    f7 = omega * m[-1] + pol * m[-1] + ka * m[-1] * a[-1] - deg * m[-2] - kd * am[-1] - alpha
    f8 = gamma * a[-1] + ka * m[-1] * a[-1] + d0 * a[-1] - delta * a[-2] - kd * am[-1] - k0
    f9 = kd * am[-1] + pol * am[-1] - ka * a[-1] * m[-1]
    fs.extend([f7, f8, f9])
    output = np.array(fs, dtype=float)
    return(output)


if __name__ == '__main__':
    n_layers = 2
    alpha = 0.1
    omega = 0.05
    deg = 1
    pol = 0.5
    ka = 2
    kd = 1
    delta = 1
    gamma = 1
    d0 = 1
    k0 = 0.1
    
    d0s = np.exp(np.linspace(-3, 2, 11))
#     d0s = [0.1, 0.15]
    
    fig, subplots = init_fig(2, 2)
    layers = np.arange(1, n_layers + 1)
    
    for d0 in d0s:
        ks = alpha, omega, deg, pol, ka, kd, delta, gamma, k0, d0
        f = partial(equil, ks=ks, n_layers=n_layers)
        x0 = np.random.normal(scale=0.01, size=3 * n_layers)
        xs = fsolve(f, x0, factor=1)
        
        xs = np.exp(xs).reshape((n_layers, int(xs.shape[0] / n_layers)))
        a = xs[:,0]
        m = xs[:,1]
        am = xs[:,2]
        t = m + am
        p = m / t
#         print('a distribution', a / a.sum())
        print('d0={}'.format(d0), ' Total microtubules', t.sum())
        print('d0={}'.format(d0), ' Proportion microtubules', t / t.sum())
        print('d0={}'.format(d0), ' Proportion open', p)
        
        subplots[0][0].scatter(np.log(d0), np.log(t.sum()))
        subplots[0][1].plot(layers, t, label=r'$d_{0}$=' + '{:.2f}'.format(d0))
        subplots[1][0].plot(layers, t / t.sum(), label=r'$d_{0}$=' + '{:.2f}'.format(d0))
        subplots[1][1].plot(layers, p, label=r'$d_{0}$=' + '{:.2f}'.format(d0))
    
    arrange_plot(subplots[0][0], xlabel=r'MCRS1 degradation constant $log(d_{0})$',
                 ylabel='log-Total number of microtubules')
    arrange_plot(subplots[0][1], xlabel='Minus-end distance to pole',
                 ylabel='Number of microtubules', showlegend=False,
                 legend_loc=1)
    arrange_plot(subplots[1][0], xlabel='Minus-end distance to pole',
                 ylabel='Proportion of microtubules')
    arrange_plot(subplots[1][1], xlabel='Minus-end distance to pole',
                 ylabel='Proportion of open minus ends')
    savefig(fig, fpath=join(PLOTS_DIR, 'test_full_model.png'))
