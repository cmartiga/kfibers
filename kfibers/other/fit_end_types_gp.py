from os.path import join

import numpy as np
import pandas as pd

from kfibers.settings import DATA_DIR
from kfibers.utils import get_model, get_posterior_df, get_summary_df


if __name__ == '__main__':
    fpath = join(DATA_DIR, 'data.csv')
    data = pd.read_csv(fpath, index_col=0)
    sel_rows = np.logical_and(data['EndType_1'] != 3, data['EndType_2'] != 3)
    data = data.loc[sel_rows, :]
    open_ends = (data[['EndType_1', 'EndType_2']] == 1).astype(int)
    
    pred_X = np.linspace(np.min(data['Relative_minus_position']),
                         np.max(data['Relative_minus_position']), 101)
    stan_data = {'open_ends': open_ends, 'num_measures': open_ends.shape[1],
                 'group_b': (data['Group'] == 'b').astype(int),
                 'num_data': data.shape[0],
                 'X': data['Relative_minus_position'],
                 'num_pred': 101, 'pred_X': pred_X}

    params = ['pred_alpha', 'pred_beta']    
    m = get_model('end_type_gp', recompile=True)
    fit = m.sampling(data=stan_data, pars=params, control={'adapt_delta': 0.9})
    df = get_posterior_df(fit, params)[0]
    summary = get_summary_df(fit)

    df.to_csv(join(DATA_DIR, 'end_types_gp.traces.csv'))
    summary.to_csv(join(DATA_DIR, 'end_types_gp.summary.csv'))
    