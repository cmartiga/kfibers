from os.path import join

import numpy as np
import pandas as pd

from kfibers.settings import DATA_DIR, PLOTS_DIR
from kfibers.plot_utils import init_fig, arrange_plot, savefig,\
    plot_post_pred_ax, create_patches_legend, PALETTE


def calc_p(x):
    lambda1 = np.exp(x)
    p = lambda1/ np.vstack([lambda1.sum(1)] * lambda1.shape[1]).transpose()
    return(p)
    

if __name__ == '__main__':
    fpath = join(DATA_DIR, 'test_minusends.csv')
    data = pd.read_csv(fpath, index_col=0)
    data['interval'] = pd.cut(data['Relative_minus_position'], bins=20)
    data['start'] = [x.left for x in data['interval']]
    data['end'] = [x.right for x in data['interval']]
    interval_size = np.mean(data['end'] - data['start'])
    pred_X = np.linspace(np.min(data['start']), np.max(data['end']), 101)
    
    fpath = join(DATA_DIR, 'test_n_ends_splines.traces.csv')
    alpha = pd.read_csv(fpath, usecols=lambda x:x.startswith('pred_alpha')).values
    beta1 = pd.read_csv(fpath, usecols=lambda x:x.startswith('pred_beta') and x.endswith('1')).values
    beta2 = pd.read_csv(fpath, usecols=lambda x:x.startswith('pred_beta') and x.endswith('2')).values
    
    qs = np.array([2.5, 10, 25, 50, 75, 90, 97.5])
    p1 = np.percentile(calc_p(alpha), q=qs, axis=0)
    p2 = np.percentile(calc_p(alpha + beta1), q=qs, axis=0) 
    p3 = np.percentile(calc_p(alpha + beta1 + beta2), q=qs, axis=0)
    
    beta1 = np.log2(calc_p(alpha + beta1) / calc_p(alpha))
    b1 = np.percentile(beta1, q=qs, axis=0)
    beta2 = np.log2(calc_p(alpha + beta1 + beta2) / calc_p(alpha + beta1))
    b2 = np.percentile(beta2, q=qs, axis=0)
    
    # Fig     
    xlims = pred_X[[0, -1]]
    xlims = (-0.2, 0.2)
    fig, subplots = init_fig(1, 2, rowsize=3, colsize=3.5)
    axes = subplots[0]
    plot_post_pred_ax(pred_X, p1*100, axes, color=PALETTE['WT'])
    plot_post_pred_ax(pred_X, p2*100, axes, color=PALETTE['siControl'])
    plot_post_pred_ax(pred_X, p3*100, axes, color=PALETTE['siMCRS1'])
    
    ylims = axes.get_ylim()
    axes.plot((0, 0), ylims, linestyle='--', c='grey', lw=0.5)
#     axes.fill_between((-0.2, 0.2), (ylims[0], ylims[0]), (ylims[1], ylims[1]),
#                   facecolor='grey', interpolate=True, alpha=0.075)
    arrange_plot(axes,  xlabel='Relative distance', xlims=xlims, ylims=ylims,
                 ylabel=r'% of minus-ends')
    create_patches_legend(axes, PALETTE, loc=2)
    
    axes = subplots[1]
    plot_post_pred_ax(pred_X, b1, axes, color=PALETTE['siControl'])
    plot_post_pred_ax(pred_X, b2, axes, color=PALETTE['siMCRS1'])
    ylims = (-8, 4)#(-2.5, 2.5)
    axes.plot((0, 0), ylims, linestyle='--', c='grey', lw=0.5)
    axes.plot(xlims, (0, 0), linestyle='--', c='grey', lw=0.5)
#     axes.fill_between((-0.2, 0.2), (ylims[0], ylims[0]), (ylims[1], ylims[1]),
#                   facecolor='grey', interpolate=True, alpha=0.075)
    arrange_plot(axes,  xlabel='Relative distance', xlims=xlims, ylims=ylims,
                 ylabel=r'$log_{2}$(OddsRatio)')
    create_patches_legend(axes, colors_dict={k: v for k,v in PALETTE.items()
                                         if k != 'WT'}, loc=4)
    
    # Savefig
    fpath = join(PLOTS_DIR, 'splines_model_n_ends.v2.png')
    savefig(fig, fpath)
    