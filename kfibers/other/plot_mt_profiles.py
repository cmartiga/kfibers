from os.path import join

from matplotlib import cm
import numpy as np
import pandas as pd
import seaborn as sns

from kfibers.settings import DATA_DIR, PLOTS_DIR
from kfibers.plots.plot_utils import arrange_plot, add_panel_label, FigGrid


if __name__ == '__main__':
    n = 15
    protein = 'kansl3'
    protein = 'mcrs1'
    
    fpath = join(DATA_DIR, 'invitro_{}.csv'.format(protein))
    data = pd.read_csv(fpath, sep=';')
#    data = pd.read_csv(fpath)
    print(data)
    s = data.groupby(['ID', 'Concentration']).count().reset_index()
    print('Number of microtubules per concentration')
    print(s.groupby(['Concentration'])['ID'].count())
#     data = data.loc[data['Experiment'] == 3, :]
    xs = np.arange(n)
    
    ylims = (-4, 1)
#     ylims2 = (0, 0.8)
#     ylims, ylims2 = None, None
    
    for mic_id, mic_data in data.groupby(['ID']):
        fig, axes = init_fig(1, 1, rowsize=2, colsize=3)
        xs = np.arange(mic_data.shape[0])
        axes.plot(xs, mic_data['Tubulin'], label='Tubulin', c='blue')
        axes.plot(xs, mic_data[protein.upper()], label=protein.upper(),
                  c='green')
        arrange_plot(axes, xlabel='Microtubule Position',
                     ylabel='Intensity', showlegend=True, legend_loc=1,
                     cols_legend=2, legend_fontsize=8)
        axes.set_xticklabels([])
        fpath = join(PLOTS_DIR, 'microtubules', '{}.{}.png'.format(protein,
                                                                   mic_id))
        savefig(fig, fpath)
