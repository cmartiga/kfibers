from os.path import join

import numpy as np
import pandas as pd
import seaborn as sns

from kfibers.settings import DATA_DIR, PLOTS_DIR
from kfibers.plot_utils import init_fig, savefig
from AS_quant.plot_utils import arrange_plot
from _functools import partial
from scipy.optimize.minpack import fsolve


def jacobian(concentrations, parameters):
    concentrations = np.exp(concentrations)
    A, B, D, E, F, G, Hn, Hc, In, Ic, S, DS1, DS2, DS3, FS, GS, BS = concentrations
    k0, l1, l2, e, s, t, di, dh, ka1, ka2, ka3, kd, s0, d0 = parameters
    
    fs = [
          # Intermediate RNA forms 
          [-l1 / e] + [0] * 16,
          [e / l1, -S * kd - e / l1] + [0] * 8 + [-B * kd] + [0] * 5 + [kd],
          [0] * 3 + [-e / l2] + [0] * 12 + [s],
          [0, e / l2, S * (ka1 + ka2 + ka3)] + [0] * 7 + [D * (ka1 + ka2 + ka3), kd, kd, kd, 0, 0, 0],
          [0, 0, 0, e / l2, -S * ka2, 0, 0, 0, 0, 0,  -F * ka2, s, 0, 0, kd, 0, 0], 
          [0] * 5 + [- S * ka1] + [0] * 4 + [- G * ka1, 0, s, 0, 0, kd, 0],
          
          # Complexes with spliceosome
          [0, S * ka1] + [0] * 8 + [B * ka1] + [0] * 5 + [-kd - s],
          [0, 0, S * ka1] + [0] * 7 + [D * ka1, -kd -s] + [0] * 5,
          [0, 0, S * ka2] + [0] * 7 + [D * ka2, 0, -kd -s] + [0] * 4,
          [0, 0, S * ka3] + [0] * 7 + [D * ka3, 0, 0, -kd -s] + [0] * 3,
          [0] * 5 + [S * ka1] + [0] * 4 + [D * ka1] + [0] * 4 + [-kd -s, 0],
          [0] * 4 + [S * ka2] + [0] * 5 + [D * ka2] + [0] * 3 + [-kd -s, 0, 0],
          
          # Spliced forms
          [0] * 8 + [-t] + [0] * 5 + [s, s, 0],
          [0] * 6 + [-t] + [0] * 6 + [s] + [0] * 3,
          [0] * 8 + [t, -di] + [0] * 7,
          [0] * 6 + [t, -dh] + [0] * 9,
          
          # Spliceosome
          [0, S * ka1, S * ka1, 0, S * ka2, S * ka1] + [0] * 4 + [(d0 + ka1 * (B + D + G) + ka2 * (D + F) + ka3  * D)] + [-kd] * 6,
          ]
    
    output = np.array(fs, dtype=float)
    return(output)


def equil(concentrations, parameters):
    concentrations = np.exp(concentrations)
    A, B, D, E, F, G, Hn, Hc, In, Ic, S, DS1, DS2, DS3, FS, GS, BS = concentrations
    k0, l1, l2, e, s, t, di, dh, ka1, ka2, ka3, kd, s0, d0 = parameters
    
    fs = [
          # Intermediate RNA forms 
          k0 - A * l1 / e,
          A * e / l1 + BS * kd - B * (S * kd + e / l1),
          BS * s - E * e / l2,
          B * e / l2 + kd * (DS1 + DS2 + DS3) - D * S * (ka1 + ka2 + ka3),
          E * e / l2 + DS1 * s + FS * kd - F * S * ka2,
          DS2 * s + GS * kd - G * S * ka1,
          
          # Complexes with spliceosome
          B * S * ka1 - BS * (kd + s),
          D * S * ka1 - DS1 * (kd + s),
          D * S * ka2 - DS2 * (kd + s),
          D * S * ka3 - DS3 * (kd + s),
          G * S * ka1 - GS * (kd + s),
          F * S * ka2 - FS * (kd + s),
          
          # Spliced forms
          s * (FS + GS) - In * t,
          s * DS3 - Hn * t,
          In * t - Ic * di,
          Hn * t - Hc * dh,
          
          # Spliceosome
          S * (d0 + ka1 * (B + D + G) + ka2 * (D + F) + ka3  * D) - kd * (BS + DS1 + DS2 + DS3 + FS + GS) - s0
          ]
    
    output = np.array(fs, dtype=float)
    return(output)


def calc_psi(concentrations, Vc_Vn):
    concentrations = np.exp(concentrations)
    A, B, D, E, F, G, Hn, Hc, In, Ic, S, DS1, DS2, DS3, FS, GS, BS = concentrations
    skipping = Hn + Hc * Vc_Vn
    inclusion = In + Ic * Vc_Vn
    total = skipping + inclusion
    return(inclusion / total)


def calc_total_spliceosome(concentrations):
    concentrations = np.exp(concentrations)
    A, B, D, E, F, G, Hn, Hc, In, Ic, S, DS1, DS2, DS3, FS, GS, BS = concentrations
    return(np.sum([S, DS1, DS2, DS3, FS, GS, BS]))


def calc_mRNA(concentrations, Vc_Vn):
    concentrations = np.exp(concentrations)
    A, B, D, E, F, G, Hn, Hc, In, Ic, S, DS1, DS2, DS3, FS, GS, BS = concentrations
    return(np.sum([Hn, Hc * Vc_Vn, In, Ic * Vc_Vn]),
           np.sum([A, B, D, E, F, G + DS1, DS2, DS3, FS, GS, BS]))
    

def optimize(f, max_iter=200, tol=1e-4):
    concs = []
    for _ in range(max_iter):
        x0 = np.random.normal(scale=0.01, size=n_components)
        concentrations = fsolve(f, x0, maxfev=200000, xtol=1e-5)
        ymean = f(concentrations).mean()
        if np.abs(ymean) < tol:
            return(concentrations)
        concs.append([ymean, concentrations])
    return(min(concs, key=lambda x:np.abs(x[0]))[1])


if __name__ == '__main__':
    species = ['A', 'B', 'D', 'E', 'F', 'G', 'Hn', 'Hc', 'In', 'Ic', 'S',
               'DS1', 'DS2', 'DS3', 'FS', 'GS', 'BS']
    
    k0 = 2
    l1=25
    l2=10
    e=5
    s=10
    t=1
    di=0.5
    dh=0.5
    ka1=5
    ka2=5
    ka3=5
    kd=5
    s0=1
    d0=5
    Vc_Vn=5

    n_components = 17
    
    parameters = k0, l1, l2, e, s, t, di, dh, ka1, ka2, ka3, kd, s0, d0
    f = partial(equil, parameters=parameters)
    concentrations = optimize(f)
    psi = calc_psi(concentrations, Vc_Vn=10)
    mrna = calc_mRNA(concentrations, Vc_Vn=10)
    splc = calc_total_spliceosome(concentrations)
    print(psi, mrna, splc)
    
    
    fig, subplots = init_fig(1, 4)
    d0s = np.linspace(0.1, 25, 15)
    l2s = np.linspace(1, 30, 15)
    k0s = np.linspace(0.1, 15, 15)
    ka3s = np.linspace(0.1, 25, 15)

    # Plot 1
    axes = subplots[0]
    psi = []
    for l2 in l2s:
        parameters = k0, l1, l2, e, s, t, di, dh, ka1, ka2, ka3, kd, s0, d0
        f = partial(equil, parameters=parameters)
        concentrations = optimize(f)
        psi.append(calc_psi(concentrations, Vc_Vn=Vc_Vn))
    axes.plot(l2s, psi)
    arrange_plot(axes, xlabel=r'$L_{2}$', ylabel=r'$\Psi$', ylims=(0, 1))
    
    # Plot 2
    axes = subplots[1]
    psi = []
    l2 = 25
    for k0 in k0s:
        parameters = k0, l1, l2, e, s, t, di, dh, ka1, ka2, ka3, kd, s0, d0
        f = partial(equil, parameters=parameters)
        concentrations = optimize(f)
        psi.append(calc_psi(concentrations, Vc_Vn=Vc_Vn))
    axes.plot(k0s, psi)
    arrange_plot(axes, xlabel=r'$k_{0}$', ylabel=r'$\Psi$', ylims=(0, 1))

    # Plot 2
    axes = subplots[2]
    psi = []
    k0 = 2
    for d0 in d0s:
        parameters = k0, l1, l2, e, s, t, di, dh, ka1, ka2, ka3, kd, s0, d0
        f = partial(equil, parameters=parameters)
        concentrations = optimize(f)
        psi.append(calc_psi(concentrations, Vc_Vn=Vc_Vn))
    axes.plot(d0s, psi)
    arrange_plot(axes, xlabel=r'$d_{0}$', ylabel=r'$\Psi$', ylims=(0, 1))
    
    # Plot 4
    axes = subplots[3]
    psi = []
    d0 = 1
    for ka3 in ka3s:
        parameters = k0, l1, l2, e, s, t, di, dh, ka1, ka2, ka3, kd, s0, d0
        f = partial(equil, parameters=parameters)
        concentrations = optimize(f)
        psi.append(calc_psi(concentrations, Vc_Vn=Vc_Vn))
    axes.plot(ka3s, psi)
    arrange_plot(axes, xlabel=r'$k_{a,3}$', ylabel=r'$\Psi$', ylims=(0, 1))

    fpath = join(PLOTS_DIR, 'splc_test.png')
    savefig(fig, fpath)
