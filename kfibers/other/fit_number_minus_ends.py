from os.path import join

import numpy as np
import pandas as pd

from kfibers.settings import DATA_DIR
from kfibers.utils import get_model, get_posterior_df, get_summary_df


if __name__ == '__main__':
    fpath = join(DATA_DIR, 'test_minusends.csv')
    data = pd.read_csv(fpath)
    data['interval'] = pd.cut(data['Relative_minus_position'], bins=20)
    data['start'] = [x.left for x in data['interval']]
    data['end'] = [x.right for x in data['interval']]
    counts = data.groupby(['start', 'Group'])['Fiber'].count().reset_index()
    counts = counts.pivot_table(index='start', columns='Group', values='Fiber', fill_value=0)
    counts['start'] = counts.index
    counts = pd.melt(counts, id_vars=['start'])
    counts['siControl'] = 0
    counts.loc[counts['Group'] != 'c', 'siControl'] = 1
    counts['siMCRS1'] = 0
    counts.loc[counts['Group'] == 'b', 'siMCRS1'] = 1
    design = counts[['siControl', 'siMCRS1']]
    
    knots = np.array(sorted(((data['start'] + data['end']) / 2).dropna().unique()))
    pred_X = np.linspace(np.min(data['start']), np.max(data['end']), 101)
    stan_data = {'num_groups': design.shape[1],
                 'num_data': counts.shape[0],
                 'num_knots': knots.shape[0],
                 'num_pred': 101,
                 
                 'knots': knots,
                 'spline_degree': 3,

                 'design': design,                 
                 'ends_number': counts['value'],
                 'X': counts['start'], 
                 
                 'pred_X': pred_X}

    params = ['pred_alpha', 'pred_beta']    
    m = get_model('n_ends_splines', recompile=False)
    fit = m.sampling(data=stan_data, pars=params, control={'adapt_delta': 0.9})
    df = get_posterior_df(fit, params)[0]
    summary = get_summary_df(fit)

    df.to_csv(join(DATA_DIR, 'test_n_ends_splines.traces.csv'))
    summary.to_csv(join(DATA_DIR, 'test_n_ends_splines.summary.csv'))
