from os.path import join

from matplotlib import cm
import numpy as np
import seaborn as sns

from kfibers.settings import PLOTS_DIR, PLOTS_FORMAT, PROTEINS, RESULTS_DIR
from kfibers.plots.plot_utils import arrange_plot, FigGrid
from kfibers.utils import align_mt_ends, read_invitro_binding, count_mt_per_conc


def make_figure(conc, left, right, left_m, right_m, max_conc, ylims=(-4, 1)):
    fig = FigGrid(figsize=(12, 2.5))
    cmap = cm.get_cmap('Purples')
    color = cmap(0.2 + 0.8 * conc / max_conc)
    xs = np.arange(n)
        
    # Left 
    axes = fig.new_axes(xend=20)
    m = left[conc]
    axes.plot(xs, m, lw=1.5, c=color, label='[{}]={} nM'.format(protein, conc))
    arrange_plot(axes, xlims=(0, n-1), ylims=ylims, despine=True,
                 xlabel='Microtubule position', showlegend=True, 
                 ylabel=r'$log_{2}$' + '({}/Tubulin)'.format(protein.upper()))
    axes.set_xticks([])
        
    # Repeat on right side
    axes = fig.new_axes(xstart=22, xend=42)
    m = right[conc]
    axes.plot(xs, m, lw=1.5, c=color)
    arrange_plot(axes, xlims=(0, n-1), ylims=ylims, 
                 xlabel='Microtubule position', ylabel='', showlegend=False)
    sns.despine(ax=axes, left=True, right=False)
    axes.set_xticks([])
        
    # Left 
    axes = fig.new_axes(xstart=58, xend=78)
    m = left_m[conc]
    axes.plot(xs, m, lw=1.5, c=color)
    arrange_plot(axes, xlims=(0, n-1), ylims=ylims,
                 xlabel='Microtubule position', showlegend=False, 
                 ylabel=r'$log_{2}$' + '({}/Tubulin)'.format(protein.upper()))
    axes.set_xticks([])
    
    # Repeat on right side
    axes = fig.new_axes(xstart=80, xend=100)
    m = right_m[conc]
    axes.plot(xs, m, lw=1.5, c=color)
    arrange_plot(axes, xlims=(0, n-1), ylims=ylims, 
                 xlabel='Microtubule position', ylabel='', showlegend=False)
    sns.despine(ax=axes, left=True, right=False)
    axes.set_xticks([])
    
    # Savefig
    fpath = join(PLOTS_DIR, 'in_vitro_binding.{}.{}.{}'.format(protein, conc,
                                                               PLOTS_FORMAT))
    fig.savefig(fpath)


if __name__ == '__main__':
    n = 15
    
    for protein in PROTEINS:
    
        data = read_invitro_binding(protein=protein)
        max_conc = data['Concentration'].max()
        counts = count_mt_per_conc(data)
        
        fpath = join(RESULTS_DIR, '{}.n_mic.tsv'.format(protein))
        counts.to_csv(fpath, sep='\t')
    
        # Align MT to ends
        left, right = align_mt_ends(data, n=n, reverse=True, take_max=False,
                                   protein=protein.upper())
        
        left_m, right_m = align_mt_ends(data, n=n, reverse=True, take_max=True,
                                        protein=protein.upper())
    
        for conc in left.keys():
            make_figure(conc, left, right, left_m, right_m, max_conc,
                        ylims=(-4, 1))
        
#     # Calc affinity constant
#     kb = calc_kb(cs=left.keys(), means=left.values())
#     axes = fig.new_axes(xstart=58, xend=78)
#     axes.plot(xs, kb)
#     arrange_plot(axes, xlims=(0, n-1), ylims=ylims2,
#                  xlabel='Microtubule position', ylabel=r'$K_{a}$')
#     axes.set_xticks([])
    
#     # Calc affinity constant
#     kb = calc_kb(cs=right.keys(), means=right.values())
#     axes = fig.new_axes(xstart=80, xend=100)
#     axes.plot(xs, kb)
#     arrange_plot(axes, xlims=(0, n-1), ylims=ylims2,
#                  xlabel='Microtubule position', ylabel='')
#     sns.despine(ax=axes, left=True, right=False)
#     axes.set_xticks([])
