from os.path import join

import numpy as np
import pandas as pd

from kfibers.settings import DATA_DIR
from kfibers.utils import get_model, get_posterior_df, get_summary_df


if __name__ == '__main__':
    fpath = join(DATA_DIR, 'test_plusends.csv')
    data = pd.read_csv(fpath)
    print(data.groupby('Group')['Relative_plus_position'].count())
    sel_rows = np.logical_and(data['EndType_1'] != 3, data['EndType_2'] != 3)
    data = data.loc[sel_rows, :]
    print(data.groupby('Group')['Relative_plus_position'].count())
    data['kfiber'] = ['{}.{}'.format(cell, fiber)
                      for cell, fiber in zip(data['Cell'], data['Fiber'])]
    open_ends = (data[['EndType_1', 'EndType_2']] == 1).astype(int)
    n_cat = np.unique(open_ends).shape[0]
    cols = ['siControl', 'siMCRS1']
    design = data[cols]
#     cells = np.vstack([(data['Cell'] == x).astype(int)
#                        for x in data['Cell'].unique()]).transpose()
    kfibers = np.vstack([(data['kfiber'] == x).astype(int)
                         for x in data['kfiber'].unique()]).transpose()
                       
    
    stan_data = {'num_data': open_ends.shape[0],
                 'num_obs': open_ends.shape[1],
                 'num_groups': design.shape[1],
#                  'num_cells': cells.shape[1],
#                  'num_fib': kfibers.shape[1],
                 
                 'design': design.astype(int),
#                  'cells': cells,
                 'open_ends': open_ends,
#                  'kfibers': kfibers,
                 }

    params = ['alpha', 'beta', 'measure_sigma', 'sigma']    
    m = get_model('end_type_binomial', recompile=False)
    fit = m.sampling(data=stan_data, pars=params, control={'adapt_delta': 0.9})
    df = get_posterior_df(fit, params)[0]
    summary = get_summary_df(fit)
    print(summary)

    df.to_csv(join(DATA_DIR, 'test_plus_end_types.traces.csv'))
    summary.to_csv(join(DATA_DIR, 'test_plus_end_types.summary.csv'))
    