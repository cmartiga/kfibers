from os.path import join

import numpy as np
import pandas as pd

from kfibers.settings import DATA_DIR
from kfibers.utils import get_model, get_posterior_df, get_summary_df


if __name__ == '__main__':
    fpath = join(DATA_DIR, 'test_minusends.csv')
    data = pd.read_csv(fpath)
    sel_rows = np.logical_and(data['EndType_1'] != 3, data['EndType_2'] != 3)
#     sel_rows = np.logical_and(sel_rows, data['Relative_minus_position'] < 0.2)
    data = data.loc[sel_rows, :]
    data['kfiber'] = ['{}.{}'.format(cell, fiber)
                      for cell, fiber in zip(data['Cell'], data['Fiber'])]
    open_ends = (data[['EndType_1', 'EndType_2']] == 1).astype(int)
    n_cat = np.unique(open_ends).shape[0]
    cols = ['siControl', 'siMCRS1']
    design = data[cols]
    kfibers = np.vstack([(data['kfiber'] == x).astype(int)
                         for x in data['kfiber'].unique()]).transpose()
                       
    
    stan_data = {'num_data': open_ends.shape[0],
                 'num_obs': open_ends.shape[1],
                 'num_groups': design.shape[1],
                 
                 'design': design.astype(int),
                 'open_ends': open_ends}

    params = ['alpha', 'beta', 'measure_sigma', 'sigma']    
    m = get_model('end_type_binomial', recompile=False)
    fit = m.sampling(data=stan_data, pars=params, control={'adapt_delta': 0.9})
    df = get_posterior_df(fit, params)[0]
    summary = get_summary_df(fit)
    print(summary)

    df.to_csv(join(DATA_DIR, 'minus_end_types.single.traces.csv'))
    summary.to_csv(join(DATA_DIR, 'minus_end_types.single.summary.csv'))
    