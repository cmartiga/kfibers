#!/usr/bin/env python
from os.path import join, exists

import pandas as pd
import numpy as np
import pystan

from _pickle import dump, load
from kfibers.settings import (MODELS_LABELS, STAN_CODE_DIR, COMPILED_DIR,
                              RESULTS_DIR, POSITION, TYPE1, TYPE2,
                              GROUP, MINUS_ENDS_FPATH, FIT_DIR, WT, CONTROL,
                              SIMCRS1, CELLS_FPATH, DATA_DIR, PLUS_ENDS_FPATH,
    INT_MINUS_ENDS_FPATH, INT_PLUS_ENDS_FPATH, INT_CELLS_FPATH)
from scipy.stats.mstats_basic import linregress


def load_pickle(fpath):
    with open(fpath, 'rb') as fhand:
        data = load(fhand)
    return(data)


def write_pickle(data, fpath):
    with open(fpath, 'wb') as fhand:
        dump(data, fhand)
    return(data)


def get_model(model_label, recompile=False):

    model = MODELS_LABELS.get(model_label, model_label)
    code_fpath = join(STAN_CODE_DIR, '{}.stan'.format(model))
    compiled_fpath = join(COMPILED_DIR, '{}.p'.format(model))

    if not exists(compiled_fpath) or recompile:
        # STAN model compilation
        model = pystan.StanModel(file=code_fpath)
        write_pickle(model, compiled_fpath)
    else:
        # Load stored model
        model = load_pickle(compiled_fpath)
    return(model)


def get_sampler_params(fit, n_iter, n_chains):
    dfs = []
    sampler_params = fit.get_sampler_params()
    for i in range(n_chains):
        df = pd.DataFrame({param: sample[int(n_iter / 2):]
                           for param, sample in sampler_params[i].items()})
        df['chain'] = i
        dfs.append(df)
    return(pd.concat(dfs, axis=0))


def get_traces_df(traces, params):
    traces_dict = {}
    for param in params:
        sample = traces[param]
        if len(sample.shape) == 2 and sample.shape[1] > 1:
            for i in range(sample.shape[1]):
                traces_dict['{}{}'.format(param, i + 1)] = sample[:, i].flatten()
        elif len(sample.shape) == 3:
            for i in range(sample.shape[1]):
                for j in range(sample.shape[2]):
                    value = sample[:, i, j]
                    traces_dict['{}{}{}'.format(param, i + 1, j + 1)] = value.flatten()
        else:
            traces_dict[param] = sample
    for k, v in traces_dict.items():
        traces_dict[k] = v.flatten()
    return(pd.DataFrame(traces_dict))


def get_posterior_df(fit, params):
    traces = fit.extract()
    log_lik = traces.get('log_lik', None)
    traces = get_traces_df(fit, params)
    return(traces, log_lik)


def get_summary_df(fit):
    fit_summary = dict(fit.summary())
    df = pd.DataFrame(fit_summary['summary'],
                      index=fit_summary['summary_rownames'],
                      columns=fit_summary['summary_colnames'])
    return(df)


def calc_cum_distrib(x, min_value=None, max_value=None, n=501):
    x = np.array(x)
    x = x[np.isnan(x) == False]
    if min_value is None:
        min_value = np.nanmin(x)
    if max_value is None:
        max_value = np.nanmax(x)
    
    x_values = np.linspace(min_value, max_value, n)
    pcum = np.array([np.nanmean(x <= v) for v in x_values])
    return(x_values, pcum) 


def invlogit(X):
    return(np.exp(X) / (1 + np.exp(X)))


def is_alpha(colname):
    return(colname.startswith('pred_alpha'))


def is_beta1(colname):
    return(colname.startswith('pred_beta') and colname.endswith('1'))


def is_beta2(colname):
    return(colname.startswith('pred_beta') and colname.endswith('2'))


def is_coeff(x):
    return(x.startswith('alpha') or x.startswith('beta'))


def load_model_fit(dataset, only_matching=False, prefix='minus_ends'):
    if only_matching:
        dataset = '{}.only_matching'.format(dataset)
    fpath = join(FIT_DIR, '{}.{}.traces.csv'.format(prefix, dataset))
    if 'minus_ends' in prefix:
        alpha = pd.read_csv(fpath, usecols=is_alpha).values 
        beta1 = pd.read_csv(fpath, usecols=is_beta1).values
        beta2 = pd.read_csv(fpath, usecols=is_beta2).values
        traces = {'alpha': alpha, 'beta1': beta1}
        if beta2.shape[0] > 1:
            traces['beta2'] = beta2
    else:
        df = pd.read_csv(fpath, usecols=is_coeff)
        traces = {col: df[col].values.flatten() for col in df.columns}
    return(traces)


def make_intervals(data, bins, quantiles=False):
    if quantiles:
        bins = np.percentile(data[POSITION],
                             q=np.linspace(0, 100, bins))   
    data['interval'] = pd.cut(data[POSITION], bins=bins,
                              include_lowest=True)
    data['start'] = [x.left for x in data['interval']]
    data['end'] = [x.right for x in data['interval']]
    return(data)


def filter_dataset(data, dataset=None):
    if dataset is None or dataset == 'full':
        return(data)
    data = data.loc[data[dataset] == 1, :]
    return(data)


def count_ends(data):
    return(data.groupby(GROUP)[GROUP].count())


def count_matching(data, endtype=None):
    matching = data[TYPE1] == data[TYPE2]
    if endtype is not None:
        matching = np.logical_and(matching, data[TYPE1] == endtype)
    data['matching type'] = matching
    return(data.groupby(GROUP)['matching type'].sum().astype(int)) 


def init_summary(data):
    counts = count_ends(data)
    total_matching = count_matching(data, endtype=None)
    matching1 = count_matching(data, endtype=1)
    matching2 = count_matching(data, endtype=2)
    summary = pd.DataFrame({'Total ends': counts, 'Matching': total_matching,
                            'Matching Open': matching1,
                            'Matching Open %': matching1 / total_matching * 100,
                            'Matching Closed': matching2,
                            'Matching Closed %': matching2 / total_matching * 100})
    return(summary)


def remove_undefined_ends(data, summary):
    sel_rows = np.logical_and(data[TYPE1] != 3, data[TYPE2] != 3)
    new_data = data.loc[sel_rows, :]
    postfilter = count_ends(new_data)

    summary['Filtered'] = postfilter
    summary['Removed Undef'] = summary['Total ends'] - postfilter
    summary['% removed Undef'] = summary['Removed Undef'] / summary['Total ends'] * 100
    return(new_data, summary)


def filter_matching(data):
    sel_rows = data['matching']
    return(data.loc[sel_rows, :])


def load_cells_data(interpolar=False):
    if interpolar:
        cells = pd.read_csv(INT_CELLS_FPATH, index_col=0)
    else:
        cells = pd.read_csv(CELLS_FPATH, index_col=0)
    return(cells)


def load_ends(plus=False, interpolar=False):
    if interpolar:
        if plus:
            data = pd.read_csv(INT_PLUS_ENDS_FPATH)
        else:
            data = pd.read_csv(INT_MINUS_ENDS_FPATH)
    else:
        if plus:
            data = pd.read_csv(PLUS_ENDS_FPATH)
        else:
            data = pd.read_csv(MINUS_ENDS_FPATH)
    return(data)


def load_ends_data(dataset='full', bins=25, quantiles=False,
                   rm_undefined=True, only_matching=False,
                   interpolar=False, plus=False):
    data = load_ends(plus=plus, interpolar=interpolar)
    cells = load_cells_data(interpolar=interpolar)
    data = data.join(cells, on='Cell')
    data = filter_dataset(data, dataset=dataset)
    data = make_intervals(data, bins=bins, quantiles=quantiles)
    
    summary = init_summary(data)
    new_data, summary = remove_undefined_ends(data, summary=summary)
    if rm_undefined:
        data = new_data
    
    fname = 'minus_ends.{}.summary.tsv'.format(dataset)
    if interpolar:
        fname = 'interpolar.' + fname
        
    fpath = join(RESULTS_DIR, fname)
    summary.to_csv(fpath, sep='\t')
    
    data['matching'] = data[TYPE1] == data[TYPE2]
    if only_matching:
        data = filter_matching(data)
    return(data)


def load_minus_ends_data(dataset='full', bins=25, quantiles=False,
                         rm_undefined=True, only_matching=False,
                         interpolar=False):
    data = load_ends_data(dataset=dataset, bins=bins, quantiles=quantiles,
                          rm_undefined=rm_undefined, only_matching=only_matching,
                          interpolar=interpolar, plus=False)
    return(data)


def load_plus_ends_data(dataset='full', rm_undefined=True, only_matching=False):
    data = pd.read_csv(PLUS_ENDS_FPATH)
    cells = load_cells_data()
    data = pd.merge(data, cells, left_on='Cell', right_index=True,
                    how='left', sort=False)
    data = filter_dataset(data, dataset=dataset)
    summary = init_summary(data)
    new_data, summary = remove_undefined_ends(data, summary=summary)
    if rm_undefined:
        data = new_data
    fpath = join(RESULTS_DIR, 'plus_ends.{}.summary.tsv'.format(dataset))
    summary.to_csv(fpath, sep='\t')
    
    data['matching'] = data[TYPE1] == data[TYPE2]
    if only_matching:
        data = filter_matching(data)
    return(data)


def get_pred_X(dataset, data=None, only_matching=False,
               interpolar=False, plus=False):
    if data is None:
        data = load_ends_data(dataset=dataset,
                              only_matching=only_matching,
                              interpolar=interpolar,
                              plus=plus)
    xs = data[POSITION]
    pred_X = np.linspace(np.min(xs), np.max(xs), 101)
    return(pred_X)


def calc_open_perc_posterior(traces):
    alpha, beta1 = traces['alpha'], traces['beta1']
    p1 = invlogit(alpha) * 100
    p2 = invlogit(alpha + beta1) * 100
    
    if 'beta2' in traces:
        beta2 = traces['beta2']
        p3 = invlogit(alpha + beta1 + beta2) * 100
        traces = {WT: p1, CONTROL: p2, SIMCRS1: p3}
    else:
        traces = {CONTROL: p1, SIMCRS1: p2}
    return(traces)


def calc_percentiles(traces, qs=np.array([2.5, 10, 25, 50, 75, 90, 97.5])):
    return({k: np.percentile(v, q=qs, axis=0) for k, v in traces.items()})


def read_invitro_binding(protein):
    fpath = join(DATA_DIR, 'invitro_{}.tsv'.format(protein.lower()))
    data = pd.read_csv(fpath, sep='\t')
    return(data)


def count_mt_per_conc(data):
    s = data.groupby(['ID', 'Concentration']).count().reset_index()
    counts = s.groupby(['Concentration'])['ID'].count()
    return(counts)


def get_alined_microtubules(data, n=20, reverse=False, take_max=False,
                            protein='MCRS1', rm_pos=5, lm_norm=False):
    aligned = {}
    for conc, conc_data in data.groupby(['Concentration']):
        
        conc_aligned = []
        for _, mic_data in conc_data.groupby(['ID']):
            tubulin = mic_data['Tubulin'].values.astype(float)
            if lm_norm:
                pos = np.arange(tubulin.shape[0])
                slope, intercept = linregress(pos[rm_pos:-rm_pos],
                                              tubulin[rm_pos:-rm_pos])[:2]
                lattice = intercept + slope * pos
            else:
                lattice = tubulin[rm_pos:-rm_pos].mean() 
            ratio = np.log2(mic_data[protein].values.astype(float) / lattice)
            if take_max:
                end1 = np.mean(ratio[:8])
                end2 = np.mean(ratio[-8:])
                if end2 > end1:
                    ratio = ratio[::-1] 
            if reverse:
                ratio = ratio[::-1]
            ratio = ratio[:n]
            conc_aligned.append(ratio)
        conc_aligned = np.vstack(conc_aligned)
        m = np.nanmean(conc_aligned, axis=0)
        if reverse:
            m = m[::-1]
        aligned[conc] = m
    return(aligned)


def align_mt_ends(data, protein, n=15, take_max=False, rm_pos=5,
                  lm_norm=False):
    left = get_alined_microtubules(data, n=n, reverse=False, take_max=take_max,
                                   protein=protein.upper(), rm_pos=rm_pos,
                                   lm_norm=lm_norm)
    right = get_alined_microtubules(data, n=n, reverse=True, take_max=take_max,
                                    protein=protein.upper(), rm_pos=rm_pos,
                                   lm_norm=lm_norm)
    return(left, right)


def calc_affinity_constant(cs, means):
    means = np.vstack(means)
    cs = np.array([c for c in cs])
    slopes = np.array([linregress(cs, 2** means[:, i])[0]
                       for i in range(means.shape[1])])
    kb = slopes / (1 - slopes)
    return(kb)


def read_microtubule_angle_OM():
    fpath = join(DATA_DIR, 'Microtubule_angle.csv')
    data = pd.read_csv(fpath)
    return(data)


def read_microtubule_angle_EM():
    fpath = join(DATA_DIR, 'Microtubule_Angle_EM.csv')
    data = pd.read_csv(fpath)
    return(data)


def read_half_spindle_angle_OM():
    fpath = join(DATA_DIR, 'Half_Spindle_Angle.csv')
    data = pd.read_csv(fpath)
    return(data)
    
    
def read_half_spindle_angle_EM():
    fpath = join(DATA_DIR, 'Half_Spindle_Angle_EM.csv')
    data = pd.read_csv(fpath)
    return(data)


def read_comp_inter_kinetocore_dist():
    fpath = join(DATA_DIR, 'ComparisonInterkinetochore.csv')
    data = pd.read_csv(fpath)
    return(data)


def read_comp_nMT():
    fpath = join(DATA_DIR, 'ComparisonNumberofMTs.csv')
    data = pd.read_csv(fpath)
    return(data)


def read_inter_kinetocore_dist():
    fpath = join(DATA_DIR, 'Interkinetochore_distance.csv')
    data = pd.read_csv(fpath)
    return(data)
