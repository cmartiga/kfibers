from os.path import join

import numpy as np
import pandas as pd
import seaborn as sns

from kfibers.settings import WT, CONTROL, SIMCRS1, FIT_DIR
from kfibers.plots.plot_utils import (init_fig, arrange_plot, savefig, PALETTE,
                                      create_patches_legend, add_panel_label)


def invlogit(X):
    return(np.exp(X) / (1 + np.exp(X)))


if __name__ == '__main__':
    cm = 1/2.54
    
    fpath = join(FIT_DIR, 'plus_ends.full.traces.csv')
    data = pd.read_csv(fpath, index_col=0)
    percentages = pd.DataFrame({WT: invlogit(data['alpha']),
                                CONTROL: invlogit(data['alpha'] + data['beta1']),
                                SIMCRS1: invlogit(data['alpha'] + data['beta1'] + data['beta2'])})* 100
    p_open = pd.melt(percentages)
    deltas = pd.DataFrame({CONTROL: data['beta1'],
                           SIMCRS1: data['beta2']})
    p_delta = (deltas > 0).mean().to_dict()
    deltas = pd.melt(deltas)
    
    # Fig
    fig, subplots = init_fig(1, 2, figsize=(16*cm, 8*cm))
    axes = subplots[0]
    sns.violinplot(x='variable', y='value', hue='variable', data=p_open,
                    palette=PALETTE, inner='quartile', ax=axes, dodge=False)
    arrange_plot(axes, ylims=(0, 105), xlabel='', despine=False,
                 ylabel='% open plus-ends', showlegend=False)
    create_patches_legend(axes, PALETTE, loc=(-0.3, 1.05), ncol=3,
                          fontsize=8)
    add_panel_label(axes, label='A', yfactor=0.25, xfactor=0.35)
    
    axes = subplots[1]
    sns.violinplot(x='variable', y='value', hue='variable', data=deltas,
                   palette=PALETTE, inner='quartile', ax=axes, dodge=False)
    arrange_plot(axes, xlabel='', despine=False, 
                 ylabel=r'$\Delta log\left(\frac{open}{closed}\right)$', showlegend=False,
                 hline=0)
    create_patches_legend(axes, {k: PALETTE[k] for k in [CONTROL, SIMCRS1]},
                          loc=(0, 1.05), ncol=3, fontsize=8)
    add_panel_label(axes, label='B', yfactor=0.25)
    
    # Savefig
    savefig(fig, 'figS2')
