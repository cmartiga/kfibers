from os.path import join

import seaborn as sns
import pandas as pd

from kfibers.plots.plot_utils import init_fig, savefig, arrange_plot, PALETTE
from kfibers.settings import RESULTS_DIR


if __name__ == '__main__':
    fpath = join(RESULTS_DIR, 'kMTs_vs_non_kMTs_silencing.percentages.csv')
    data = pd.read_csv(fpath)

    fig, axes = init_fig(1, 1, colsize=2.2, rowsize=2.5)
    
    sns.barplot(x='Group', y='Open_percentage', hue='Treatment', 
                palette=PALETTE, data=data, n_boot=1, linewidth=1,
                edgecolor='black', order=['kMTs', 'non-kMTs'])
    arrange_plot(axes, showlegend=True, xlabel='', ylims=(0, 130),
                 ylabel='Open minus-ends (%)')
    axes.set_xticklabels(['KMTs', 'Non-KMTs'])
    
    savefig(fig, 'kMTs_vs_non_kMTs')
    