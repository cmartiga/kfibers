from kfibers.settings import GROUPS, POSITION
from kfibers.plots.plot_utils import (init_fig, savefig,
                                      plot_relative_position_distrib,
                                      plot_percentages_posterior,
                                      plot_params_posterior)
from kfibers.utils import (get_pred_X, load_model_fit, calc_open_perc_posterior,
                           load_ends_data, calc_percentiles)


def make_figure(dataset, perc_qs, params_qs, pred_X, only_matching=False,
                xlabel='Pole distance', between=(0, 2)):
    data = load_ends_data(dataset, interpolar=True)

    xlims = data[POSITION].min(), data[POSITION].max()
    fig, subplots = init_fig(3, 1, rowsize=3, colsize=3.5)
    plot_relative_position_distrib(data, axes=subplots[0], groups=GROUPS,
                                   xlims=xlims, xlabel=xlabel,
                                   between=between)
    plot_percentages_posterior(pred_X, perc_qs, axes=subplots[1], xlims=xlims,
                               xlabel=xlabel,
                               between=between)
    plot_params_posterior(pred_X, params_qs, axes=subplots[2], xlims=xlims, 
                          xlabel=xlabel,
                          between=between)
    
    # Savefig
    if only_matching:
        dataset = '{}.only_matching'.format(dataset)
    savefig(fig, fname='interpolar.minus_ends.splines.{}'.format(dataset))


if __name__ == '__main__':
    only_matching = False
    dataset = 'full'
    
    pred_X = get_pred_X(dataset, only_matching=only_matching, interpolar=True)
    traces = load_model_fit(dataset, only_matching=only_matching, 
                            prefix='interpolar.minus_ends')
    
    percentages = calc_open_perc_posterior(traces)
    params_qs = calc_percentiles(traces)
    perc_qs = calc_percentiles(percentages)
    make_figure(dataset, perc_qs=perc_qs, params_qs=params_qs,
                pred_X=pred_X, only_matching=only_matching)
