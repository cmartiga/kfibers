from kfibers.settings import DATASETS, WT
from kfibers.plots.plot_utils import (init_fig, savefig, plot_relative_position_distrib,
                                      plot_percentages_posterior)
from kfibers.utils import (load_minus_ends_data, get_pred_X, load_model_fit,
                           calc_open_perc_posterior, calc_percentiles)


def make_figure(data, dataset, perc_qs, pred_X, only_matching=False):
    xlims = [-0.15, 0.93]
    fig, subplots = init_fig(1, 2, rowsize=2.8, colsize=3.5)
    
    axes = subplots[0]
    plot_relative_position_distrib(data, axes=axes, groups=[WT],
                                   xlims=xlims, kde=False, grey_area=True)
    
    axes = subplots[1]
    plot_percentages_posterior(pred_X, perc_qs, axes=axes, xlims=xlims)
    axes = axes.twinx()
    plot_relative_position_distrib(data, axes=axes, groups=[WT],
                                   xlims=xlims, kde=False, grey_area=False)
    
    # Savefig
    if only_matching:
        dataset = '{}.only_matching'.format(dataset)
    savefig(fig, fname='minus_ends.splines.{}.wt'.format(dataset))


if __name__ == '__main__':
    dataset = DATASETS[0]
    for only_matching in [False, True]:
        data = load_minus_ends_data(dataset=dataset, rm_undefined=False,
                                    only_matching=only_matching)
        pred_X = get_pred_X(dataset, data=data, only_matching=only_matching)
        traces = load_model_fit(dataset, only_matching=only_matching)
        percentages = calc_open_perc_posterior(traces)
        perc_qs = {WT: calc_percentiles(percentages)[WT]}
        make_figure(data, dataset, perc_qs=perc_qs, pred_X=pred_X,
                    only_matching=only_matching)
    