import seaborn as sns
import numpy as np

from kfibers.plots.plot_utils import init_fig, savefig, arrange_plot
from kfibers.utils import load_ends_data


if __name__ == '__main__':
    only_matching = False
    dataset = 'full'
    
    data = load_ends_data(dataset=dataset, only_matching=only_matching,
                          interpolar=True, plus=False)
    sel_rows = np.logical_or(data['Pole_association'] == 'Pole1',
                             data['Cell'] <= 2)
    data = data.loc[sel_rows, :] 
    
    fig, axes = init_fig(1, 1)
    
    for cell, values in data.groupby('Cell')['Relative_position']: 
        sns.kdeplot(values, label=cell)
    arrange_plot(axes, showlegend=True, xlabel='Pole distance')
        
    savefig(fig, 'interpolar_minus_ends')