#!/usr/bin/env python
import numpy as np
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.gridspec import GridSpec
import matplotlib

from kfibers.settings import (WT, CONTROL, SIMCRS1, GROUP, POSITION, GROUPS,
                              PLOTS_DIR, PLOTS_FORMAT, EXT_PLOTS_DIR)
from os.path import join

PALETTE = {WT: (138/255., 40/255., 99/255., 1),
           CONTROL: (246/255., 172/255., 90/255., 1),
           SIMCRS1: (5/255., 172/255., 114/255., 1)}
matplotlib.rcParams['font.family'] = 'sans-serif'

# Functions
def init_fig(nrow=1, ncol=1, figsize=None, style='ticks',
             colsize=3, rowsize=3):
    sns.set_style(style)
    if figsize is None:
        figsize = (colsize * ncol, rowsize * nrow)
    fig, axes = plt.subplots(nrow, ncol, figsize=figsize)
    return(fig, axes)


def savefig(fig, fname, tight=True):
    fpath = join(PLOTS_DIR, '{}.{}'.format(fname, PLOTS_FORMAT))
    if tight:
        fig.tight_layout()
    fig.savefig(fpath, format=PLOTS_FORMAT, dpi=240)
    plt.close()


def add_image(axes, fname):
    fmt = fname.split('.')[-1]
    arr_image = plt.imread(join(EXT_PLOTS_DIR, fname), format=fmt)    
    axes.imshow(arr_image)
    empty_axes(axes)


def plot_comp_line(axes, x1, x2, y, size, lw=1):
    axes.plot((x1, x2), (y, y), lw=lw, c='black')
    axes.plot((x1, x1), (y-size, y), lw=lw, c='black')
    axes.plot((x2, x2), (y-size, y), lw=lw, c='black')


def empty_axes(axes):
    sns.despine(ax=axes, left=True, bottom=True)
    axes.set_xticks([])
    axes.set_yticks([])


def create_patches_legend(axes, colors_dict, loc=1, **kwargs):
    axes.legend(handles=[mpatches.Patch(color=color, label=label)
                         for label, color in colors_dict.items()],
                loc=loc, **kwargs)


def set_boxplot_colorlines(axes, color):
    # From
    # https://stackoverflow.com/questions/36874697/how-to-edit-properties-of-whiskers-fliers-caps-etc-in-seaborn-boxplot
    for i, artist in enumerate(axes.artists):
        artist.set_edgecolor(color)
        artist.set_facecolor('None')

        # Each box has 6 associated Line2D objects (to make the whiskers, fliers, etc.)
        # Loop over them here, and use the same colour as above
        for line in axes.lines:
            line.set_color(color)
            line.set_mfc(color)
            line.set_mec(color)


def arrange_plot(axes, xlims=None, ylims=None, xlabel=None, ylabel=None,
                 showlegend=False, legend_loc=None, hline=None, vline=None,
                 rotate_xlabels=False, cols_legend=1, rotation=90,
                 legend_frame=True, title=None, ticklabels_size=None,
                 yticklines=False, despine=False, legend_fontsize=10):
    if xlims is not None:
        axes.set_xlim(xlims)
    if ylims is not None:
        axes.set_ylim(ylims)
    if title is not None:
        axes.set_title(title)

    if xlabel is not None:
        axes.set_xlabel(xlabel)
    if ylabel is not None:
        axes.set_ylabel(ylabel)

    if showlegend:
        axes.legend(loc=legend_loc, ncol=cols_legend,
                    frameon=legend_frame, fancybox=legend_frame,
                    fontsize=legend_fontsize)
    elif axes.legend_ is not None:
        axes.legend_.set_visible(False)

    if hline is not None:
        xlims = axes.get_xlim()
        axes.plot(xlims, (hline, hline), linewidth=1, color='grey',
                  linestyle='--')
        axes.set_xlim(xlims)

    if vline is not None:
        ylims = axes.get_ylim()
        axes.plot((vline, vline), ylims, linewidth=1, color='black',
                  linestyle='--')
        axes.set_ylim(ylims)

    if rotate_xlabels:
        axes.set_xticklabels(axes.get_xticklabels(), rotation=rotation,
                             ha='right')
    if ticklabels_size is not None:
        for tick in axes.xaxis.get_major_ticks():
            tick.label.set_fontsize(ticklabels_size)
        for tick in axes.yaxis.get_major_ticks():
            tick.label.set_fontsize(ticklabels_size)
    if yticklines:
        xlims = axes.get_xlim()
        for y in axes.get_yticks():
            axes.plot(xlims, (y, y), lw=0.2, alpha=0.1, c='lightgrey')

    if despine:
        sns.despine(ax=axes)


def plot_post_pred_ax(x, q, axes, color, linestyle='-'):
    for i in range(int((q.shape[0] - 1) / 2)):
        axes.fill_between(x, q[i, :], q[-(i + 1), :], facecolor=color,
                          interpolate=True, alpha=0.1)
    axes.plot(x, q[int(q.shape[0] / 2), :], color=color, linewidth=2,
              linestyle=linestyle)


def add_panel_label(axes, label, fontsize=20, yfactor=0.015, xfactor=0.35):
    xlims, ylims = axes.get_xlim(), axes.get_ylim()
    x = xlims[0] - (xlims[1] - xlims[0]) * xfactor
    y = ylims[1] + (ylims[1] - ylims[0]) * yfactor
    axes.text(x, y, label, fontsize=fontsize)


def add_grey_area(axes, between=(-0.2, 0.2), ylims=None, add_vline=True):
    if ylims is None:
        ylims = axes.get_ylim()
    axes.fill_between(between, (ylims[0], ylims[0]), (ylims[1], ylims[1]),
                      facecolor='grey', interpolate=True, alpha=0.2)
    if add_vline:
        axes.plot((0, 0), ylims, linestyle='--', c='grey', lw=0.5)


def plot_splines_pred(pred_X, percentiles, axes, linestyle='-'):
    for group, values in percentiles.items():
        plot_post_pred_ax(pred_X, values, axes, color=PALETTE[group],
                          linestyle=linestyle)


def plot_relative_position_distrib(data, axes, groups=GROUPS,
                                   xlims=[-0.15, 0.93], kde=True,
                                   grey_area=True, xlabel='Relative distance',
                                   between=(-0.2, 0.2)):
    bins = np.linspace(xlims[0], xlims[1], 16)
    present = data[GROUP].unique()
    groups = [g for g in groups if g in present]
    for group in groups:
        x = data.loc[data[GROUP] == group, POSITION]
        sns.distplot(x, color=PALETTE[group], hist=True, kde=False,
                     bins=bins, ax=axes, label=group)
    
    ylims = axes.get_ylim()
    if grey_area:
        add_grey_area(axes, ylims=ylims, between=between)
    arrange_plot(axes, xlims=xlims, xlabel=xlabel, ylims=ylims,
                 ylabel='Number of minus-ends', showlegend=False, despine=False)
    
    if kde:
        axes = axes.twinx()
        for group in groups:
            x = data.loc[data[GROUP] == group, POSITION]
            sns.kdeplot(x, color=PALETTE[group], ax=axes, label=group)
        
        ylims = axes.get_ylim()
        arrange_plot(axes, xlims=xlims, xlabel=xlabel, ylims=ylims,
                     ylabel='', showlegend=False, legend_loc=1, despine=False)
        axes.set_yticks([])
    
    if len(groups) > 1:
        create_patches_legend(axes, {k: PALETTE[k] for k in groups},
                              loc=1, fontsize=9)
    

def plot_relative_position_distrib_kde(data, axes, groups=GROUPS,
                                       xlims=[-0.15, 0.93],
                                       grey_area=True,
                                       xlabel='Relative distance',
                                       between=(-0.2, 0.2)):
    present = data[GROUP].unique()
    groups = [g for g in groups if g in present]
    for group in groups:
        x = data.loc[data[GROUP] == group, POSITION]
        sns.distplot(x, color=PALETTE[group], hist=False, kde=True,
                     ax=axes, label=group)
    ylims = axes.get_ylim()
    if grey_area:
        add_grey_area(axes, ylims=ylims, between=between)
    arrange_plot(axes, xlims=xlims, xlabel=xlabel, ylims=ylims,
                 ylabel='Density of minus-ends', showlegend=False, despine=False)
    if len(groups) > 1:
        create_patches_legend(axes, {k: PALETTE[k] for k in groups},
                              loc=1, fontsize=9)
    

def plot_percentages_posterior(pred_X, percentiles, axes, xlims,
                               xlabel='Relative distance',
                               between=(-0.2, 0.2), linestyle='-'):
    plot_splines_pred(pred_X, percentiles, axes, linestyle=linestyle)
    ylims = (0, 100)
    add_grey_area(axes, ylims=ylims, between=between)
    arrange_plot(axes,  xlabel=xlabel, xlims=xlims, ylims=ylims,
                 ylabel=r'Open minus-ends (%)', despine=False)
    if len(percentiles) > 1:
        create_patches_legend(axes, {k: PALETTE[k] for k in percentiles.keys()},
                              loc=3, fontsize=9)


def plot_params_posterior(pred_X, percentiles, axes, xlims, xlabel='Relative distance',
                          between=(-0.2, 0.2), ylims=None, linestyle='-',
                          legend=True):
    if 'beta2' in percentiles:
        plot_post_pred_ax(pred_X, percentiles['beta1'], axes, color=PALETTE[CONTROL],
                          linestyle=linestyle)
        plot_post_pred_ax(pred_X, percentiles['beta2'], axes, color=PALETTE[SIMCRS1],
                          linestyle=linestyle)
        groups = [CONTROL, SIMCRS1]
    else:
        plot_post_pred_ax(pred_X, percentiles['beta1'], axes, color=PALETTE[SIMCRS1],
                          linestyle=linestyle)
        groups = [SIMCRS1]
    
    if ylims is None:
        ylims = axes.get_ylim()
    axes.plot(xlims, (0, 0), linestyle='--', c='grey', lw=0.5)
    add_grey_area(axes, ylims=ylims, between=between)
    arrange_plot(axes,  xlabel=xlabel, xlims=xlims, ylims=ylims,
                 ylabel=r'log(Open/Closed)', despine=False)
    if legend:
        create_patches_legend(axes, {k: PALETTE[k] for k in groups},
                              loc=3, fontsize=9)
    

def _add_mean_sd_bars(axes, x, values, w=0.5):
    m = np.mean(values)
    sd = np.std(values)
    l, u = m - sd, m + sd
    
    xs = x - 0.5  * w, x + 0.5 * w
    axes.plot(xs, (m, m), linewidth=1, c='black')
    axes.plot((x, x), (l, u), linewidth=1, c='black')
    
    xs = x - 0.3  * w, x + 0.3 * w
    for y in (l, u):
        axes.plot(xs, (y, y), linewidth=1, c='black')
    

def plot_MTs_per_fiber(data, axes, stripplot=False):
    sns.boxplot(x=GROUP, y='n', hue=GROUP, data=data, order=GROUPS,
                palette=PALETTE, dodge=False, fliersize=0, ax=axes)
    if stripplot:
        sns.stripplot(x=GROUP, y='n', hue=GROUP, data=data, order=GROUPS, 
                      jitter=0.2, dodge=False, palette=PALETTE, size=4,
                      edgecolor='black', linewidth=1, ax=axes, alpha=0.1)
    arrange_plot(axes, xlabel='', ylabel='Number of KMTs', ylims=(0, 20),
                 showlegend=False, despine=False)
    
    plot_comp_line(axes, x1=0, x2=0.95, y=17, size=0.75, lw=1.2)
    plot_comp_line(axes, x1=1.05, x2=2, y=17, size=0.75, lw=1.2)
    axes.text(0.5, 18, 'n.s.', ha='center')
    axes.text(1.5, 18, '***', ha='center')


def add_mean_sd_bars(axes, x, y, data, w=0.5, order=None):
    if order is None:
        groups = [(i, values) for i, (_, values) in enumerate(data.groupby(x)[y])]
    else:
        groups = [(i, data.loc[data[x] == v, y]) for i, v in enumerate(order)]
    for x, values in groups:
        _add_mean_sd_bars(axes, x=x, values=values, w=w)


class FigGrid(object):
    def __init__(self, figsize=(11, 9.5), xsize=100, ysize=100):
        self.fig = plt.figure(figsize=figsize)
        self.gs = GridSpec(xsize, ysize, wspace=1, hspace=1)
        self.xsize = 100
        self.ysize = 100

    def new_axes(self, xstart=0, xend=None, ystart=0, yend=None):
        if xend is None:
            xend = self.xsize
        if yend is None:
            yend = self.ysize
        return(self.fig.add_subplot(self.gs[ystart:yend, xstart:xend]))

    def savefig(self, fname):
        savefig(self.fig, fname, tight=False)
