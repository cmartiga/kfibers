import seaborn as sns

from kfibers.settings import DATASETS, GROUP, TYPE1, CELL, FIBER, GROUPS
from kfibers.plots.plot_utils import (init_fig, arrange_plot, savefig, PALETTE,
                                      plot_comp_line)
from kfibers.utils import load_minus_ends_data


def count_MT_per_fiber(data):
    data['ID'] = ['{}.{}'.format(c, f) for c, f in zip(data[CELL], data[FIBER])]
    c = data.groupby([CELL, 'ID', GROUP])[TYPE1].count().reset_index()
    c.columns = [CELL, FIBER, GROUP, 'n']
    return(c)


def plot_MTs_per_fiber(data, dataset):
    fig, axes = init_fig(1, 1, rowsize=2.4, colsize=3.2)
    
    sns.boxplot(x=GROUP, y='n', hue=GROUP, data=data, order=GROUPS,
                palette=PALETTE, dodge=False, fliersize=0, ax=axes)
#     sns.stripplot(x=GROUP, y='n', hue=GROUP, data=data, order=GROUPS, 
#                   jitter=0.2, dodge=False, palette=PALETTE, size=4,
#                   edgecolor='black', linewidth=1, ax=axes, alpha=0.1)
    arrange_plot(axes, xlabel='', ylabel='Number of KMTs', ylims=(0, 20),
                 showlegend=False, despine=False)
    
    plot_comp_line(axes, x1=0, x2=0.95, y=17, size=0.75, lw=1.2)
    plot_comp_line(axes, x1=1.05, x2=2, y=17, size=0.75, lw=1.2)
    axes.text(0.5, 18, 'n.s.', ha='center')
    axes.text(1.5, 18, '***', ha='center')
    savefig(fig, fname='MTs_per_fiber.{}'.format(dataset))

if __name__ == '__main__':
    for dataset in DATASETS:
        data = load_minus_ends_data(dataset=dataset, rm_undefined=False)
        counts = count_MT_per_fiber(data)
        plot_MTs_per_fiber(counts, dataset)
        