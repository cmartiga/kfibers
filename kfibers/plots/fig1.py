from kfibers.settings import DATASETS, WT
from kfibers.plots.plot_utils import (init_fig, savefig, plot_relative_position_distrib,
                                      plot_percentages_posterior, arrange_plot)
from kfibers.utils import (load_minus_ends_data, get_pred_X, load_model_fit,
                           calc_open_perc_posterior, calc_percentiles)


def get_data(interpolar=False):
    prefix = 'minus_ends'
    if interpolar:
        prefix = 'interpolar.' + prefix
        
    minus_ends = load_minus_ends_data(dataset=dataset, rm_undefined=False,
                                only_matching=False, interpolar=interpolar)
    pred_X = get_pred_X(dataset, data=minus_ends, only_matching=False)
    traces = load_model_fit(dataset, only_matching=False, prefix=prefix)
    percentages = calc_open_perc_posterior(traces)
    perc_qs = {WT: calc_percentiles(percentages)[WT]}
    return(minus_ends, pred_X, perc_qs)


if __name__ == '__main__':
    cm = 1/2.54
    dataset = 'full'
    fig, subplots = init_fig(2, 1, figsize=(10*cm, 14*cm))
    
    # Plot kMTs in WT cells
    axes = subplots[0]
    minus_ends, pred_X, perc_qs = get_data(interpolar=False)
    plot_percentages_posterior(pred_X, perc_qs, axes=axes, xlims=(-0.15, 0.93))
    axes = axes.twinx()
    plot_relative_position_distrib(minus_ends, axes=axes, groups=[WT],
                                   xlims=(-0.15, 0.93), kde=False,
                                   grey_area=False)
    arrange_plot(axes, title='KMT minus-ends', ylabel='Number of minus-ends',
                 ylims=(0, 350))
    
    # Plot non-kMTs in WT cells
    axes = subplots[1]
    minus_ends, pred_X, perc_qs = get_data(interpolar=True)
    plot_percentages_posterior(pred_X, perc_qs, axes=axes, xlims=(0, 7),
                               linestyle='--', xlabel=r'Relative distance')
    axes = axes.twinx()
    plot_relative_position_distrib(minus_ends, axes=axes, groups=[WT],
                                   xlims=(-0.15, 0.93), kde=False, grey_area=False)
    arrange_plot(axes, ylims=(0, 350), title='Non-KMT minus-ends')
    
    # Savefig
    savefig(fig, fname='fig1')
