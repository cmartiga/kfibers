import seaborn as sns

from kfibers.plots.plot_utils import (init_fig, savefig, add_panel_label,
                                      arrange_plot)
from kfibers.utils import (read_comp_inter_kinetocore_dist,
                           read_comp_nMT)


if __name__ == '__main__':
    cm = 1/2.54
    inter_kinetocore_dist = read_comp_inter_kinetocore_dist()
    n_mic = read_comp_nMT()
    
    fig, subplots = init_fig(1, 2, figsize=(16*cm, 8*cm))

    axes = subplots[0]
    sns.boxplot(x='Characteristic', y=r'Interkinetochore distance ($\mu$m)',
                data=inter_kinetocore_dist, ax=axes, showfliers=False)
    arrange_plot(axes, xlabel='', ylabel=r'Interkinetochore distance ($\mu$m)',
                 rotate_xlabels=True, rotation=45)
    add_panel_label(axes, label='A')
     
    axes = subplots[1]
    sns.boxplot(x='Characteristic', y='Number of MTs per k-fiber', data=n_mic,
                ax=axes, showfliers=False)
    arrange_plot(axes, xlabel='', rotate_xlabels=True, rotation=45)
    add_panel_label(axes, label='B')
    
    savefig(fig, 'figS1')
        