from os.path import join

from matplotlib import cm
import numpy as np
import seaborn as sns

from kfibers.settings import PROTEINS, RESULTS_DIR, PROTEIN_CONC
from kfibers.plots.plot_utils import arrange_plot, FigGrid
from kfibers.utils import align_mt_ends, read_invitro_binding, count_mt_per_conc


def make_figure(left, right, left_m, right_m, max_conc, ylims=(-4, 1), conc=None,
                label=None):
    fig = FigGrid(figsize=(12, 2.5))
    cmap = cm.get_cmap('Purples')
    xs = np.arange(n)
    
    if conc is None:
        conc = left.keys()
    if label is None:
        label = [x for x in conc]
        
    # Left 
    axes = fig.new_axes(xend=20)
    
    for c in conc:
        color = cmap(0.2 + 0.8 * c / max_conc)
        axes.plot(xs, left[c], lw=1.5, c=color, label='[{}]={} nM'.format(protein, c))
        
    arrange_plot(axes, xlims=(0, n-1), ylims=ylims, despine=True,
                 xlabel='Microtubule position', showlegend=True, 
                 ylabel=r'$log_{2}$' + '({}/Tubulin)'.format(protein.upper()),
                 legend_fontsize=8)
    axes.set_xticks([])
        
    # Repeat on right side
    axes = fig.new_axes(xstart=22, xend=42)
    for c in conc:
        color = cmap(0.2 + 0.8 * c / max_conc)
        axes.plot(xs, right[c], lw=1.5, c=color)
    arrange_plot(axes, xlims=(0, n-1), ylims=ylims, 
                 xlabel='Microtubule position', ylabel='', showlegend=False)
    sns.despine(ax=axes, left=True, right=False)
    axes.set_xticks([])
        
    # Left 
    axes = fig.new_axes(xstart=58, xend=78)
    for c in conc:
        color = cmap(0.2 + 0.8 * c / max_conc)
        axes.plot(xs, left_m[c], lw=1.5, c=color)
    arrange_plot(axes, xlims=(0, n-1), ylims=ylims, despine=True,
                 xlabel='Microtubule position', showlegend=False, 
                 ylabel=r'$log_{2}$' + '({}/Tubulin)'.format(protein.upper()))
    axes.set_xticks([])
    
    # Repeat on right side
    axes = fig.new_axes(xstart=80, xend=100)
    for c in conc:
        color = cmap(0.2 + 0.8 * c / max_conc)
        axes.plot(xs, right_m[c], lw=1.5, c=color)
    arrange_plot(axes, xlims=(0, n-1), ylims=ylims, 
                 xlabel='Microtubule position', ylabel='', showlegend=False)
    sns.despine(ax=axes, left=True, right=False)
    axes.set_xticks([])
    
    # Savefig
    fig.savefig(fname='in_vitro_binding.{}.{}'.format(protein, label))


if __name__ == '__main__':
    n = 15
    for protein in PROTEINS:
        # Load data
        data = read_invitro_binding(protein=protein)
        max_conc = data['Concentration'].max()
        counts = count_mt_per_conc(data)
        
        # Save summary
        fpath = join(RESULTS_DIR, '{}.n_mic.tsv'.format(protein))
        counts.to_csv(fpath, sep='\t')
    
        # Align MT to ends
        left, right = align_mt_ends(data, protein=protein, n=n,
                                    take_max=False)
        
        left_m, right_m = align_mt_ends(data, protein=protein, n=n,
                                        take_max=True)
    
        # Make an independent plot for each concentration
        for conc in left.keys():
            make_figure(left, right, left_m, right_m, max_conc,
                        ylims=(-4, 1), conc=[conc], label=conc)
        
        # Make joint plot
        make_figure(left, right, left_m, right_m, max_conc,
                    ylims=(-4, 1), conc=PROTEIN_CONC[protein],
                    label='full')
