import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl

from kfibers.plots.plot_utils import (arrange_plot, savefig, PALETTE,
                                      plot_comp_line, add_panel_label, FigGrid,
                                      add_image, add_mean_sd_bars)
from kfibers.utils import (read_half_spindle_angle_OM,
                           read_microtubule_angle_OM, read_microtubule_angle_EM,
                           read_half_spindle_angle_EM)


def plot_half_spindle_angle_OM(data, axes):
    sns.boxplot(x='Genotype', y='Microtubule Angle', hue='Genotype',
                data=data, palette=PALETTE, ax=axes, showfliers=False,
                dodge=False,)
    plot_comp_line(axes, x1=0, x2=0.95, y=110, size=2, lw=1.2)
    axes.text(0.5, 110, '***', ha='center', fontsize=12)
    arrange_plot(axes, ylims=(50, 120), xlabel='',
                 ylabel='Half spindle angle (º)', showlegend=False)
    add_panel_label(axes, 'D', xfactor=0.4)
    


def plot_half_spindle_angle_EM(data, axes):
#     sns.boxplot(x='Condition', y='Half Spindle Angle', hue='Condition',
#                 dodge=False, data=data, palette=PALETTE, ax=axes,
#                 showfliers=False)
    sns.stripplot(x='Condition', y='Half Spindle Angle', hue='Condition',
                  data=data, jitter=0.2, dodge=False, palette=PALETTE,
                  size=4, edgecolor='black', linewidth=1, ax=axes)
    add_mean_sd_bars(axes, x='Condition', y='Half Spindle Angle', data=data,
                     order=['siScramble', 'siMCRS1'])
    
    plot_comp_line(axes, x1=0, x2=0.95, y=105, size=2, lw=1.2)
    axes.text(0.5, 105, '***', ha='center', fontsize=12)
    arrange_plot(axes, ylims=(50, 120), xlabel='',
                 ylabel='Half spindle angle (º)', showlegend=False)
    add_panel_label(axes, 'E', xfactor=0.4)


def plot_microtubule_angle_OM(data, axes):
    sns.boxplot(x='Genotype', y='Angle', hue='Genotype', dodge=False,
                data=data, palette=PALETTE, ax=axes, showfliers=False)
    plot_comp_line(axes, x1=0, x2=0.95, y=200, size=2, lw=1.2)
    axes.text(0.5, 200, '***', ha='center', fontsize=12)
    arrange_plot(axes, ylims=(120, 215), xlabel='',
                 ylabel='Microtubule angle (º)', showlegend=False)
    add_panel_label(axes, 'F', xfactor=0.4)
    

def plot_microtubule_angle_EM(data, axes):
    sns.stripplot(x='Condition', y='Microtubule Angle', hue='Condition',
                  data=data, jitter=0.2, dodge=False, palette=PALETTE,
                  size=4, edgecolor='black', linewidth=1, ax=axes)
    add_mean_sd_bars(axes, x='Condition', y='Microtubule Angle', data=data,
                     order=['siScramble', 'siMCRS1'])
    
    plot_comp_line(axes, x1=0, x2=0.95, y=180, size=2, lw=1.2)
    axes.text(0.5, 180, '***', ha='center', fontsize=12)
    arrange_plot(axes, ylims=(120, 215), xlabel='',
                 ylabel='Microtubule angle (º)', showlegend=False)
    add_panel_label(axes, 'G', xfactor=0.4)    


if __name__ == '__main__':
    cm = 1/2.54
    fig = FigGrid(figsize=(16*cm, 32*cm))

    # read image file
    axes = fig.new_axes(xstart=0, xend=60, yend=25)
    add_image(axes, fname='figS1A.png')
    add_panel_label(axes, label='A')
    
    axes = fig.new_axes(xstart=50, xend=100, yend=25)
    add_image(axes, fname='figS1B.png')
    add_panel_label(axes, label='B', xfactor=0.1)

    axes = fig.new_axes(xstart=0, xend=50, ystart=28, yend=47)
    add_image(axes, fname='figS1C1.png')
    add_panel_label(axes, label='C', xfactor=0.65)
    
    axes = fig.new_axes(xstart=50, xend=100, ystart=28, yend=47)
    add_image(axes, fname='figS1C2.png')

    
    axes = fig.new_axes(xstart=5, xend=40, ystart=50, yend=70)
    data = read_half_spindle_angle_OM()
    plot_half_spindle_angle_OM(data, axes)

    axes = fig.new_axes(xstart=60, xend=95, ystart=50, yend=70)
    data = read_half_spindle_angle_EM()
    plot_half_spindle_angle_EM(data, axes)
     
    axes = fig.new_axes(xstart=5, xend=40, ystart=80, yend=100)
    data = read_microtubule_angle_OM()
    plot_microtubule_angle_OM(data, axes)
    
    axes = fig.new_axes(xstart=60, xend=95, ystart=80, yend=100) 
    data = read_microtubule_angle_EM()
    plot_microtubule_angle_EM(data, axes)
    
    fig.savefig(fname='figS4')
        