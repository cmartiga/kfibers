from kfibers.settings import DATASETS, GROUPS
from kfibers.plots.plot_utils import (init_fig, savefig,
                                      plot_relative_position_distrib,
                                      plot_percentages_posterior,
                                      plot_params_posterior)
from kfibers.utils import (get_pred_X, load_model_fit, calc_open_perc_posterior,
                           load_minus_ends_data, calc_percentiles)


def make_figure(dataset, perc_qs, params_qs, pred_X, only_matching=False):
    data = load_minus_ends_data(dataset)

    xlims = [-0.15, 0.93]
    fig, subplots = init_fig(3, 1, rowsize=3, colsize=3.5)
    plot_relative_position_distrib(data, axes=subplots[0], groups=GROUPS,
                                   xlims=xlims)
    plot_percentages_posterior(pred_X, perc_qs, axes=subplots[1], xlims=xlims)
    plot_params_posterior(pred_X, params_qs, axes=subplots[2], xlims=xlims)
    
    # Savefig
    if only_matching:
        dataset = '{}.only_matching'.format(dataset)
    savefig(fig, fname='minus_ends.splines.{}'.format(dataset))


if __name__ == '__main__':
    for only_matching in [True, False]:
        for dataset in DATASETS:
            pred_X = get_pred_X(dataset, only_matching=only_matching)
            traces = load_model_fit(dataset, only_matching=only_matching)
            percentages = calc_open_perc_posterior(traces)
            params_qs = calc_percentiles(traces)
            perc_qs = calc_percentiles(percentages)
            make_figure(dataset, perc_qs=perc_qs, params_qs=params_qs,
                        pred_X=pred_X, only_matching=only_matching)
