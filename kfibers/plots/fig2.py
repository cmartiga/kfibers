from kfibers.plots.plot_utils import (plot_relative_position_distrib_kde,
                                      plot_MTs_per_fiber, init_fig, savefig,
                                      add_panel_label)
from kfibers.utils import load_minus_ends_data
from kfibers.settings import GROUPS
from kfibers.plots.plot_MTs_per_fiber import count_MT_per_fiber


if __name__ == '__main__':
    cm = 1/2.54
    fig, subplots = init_fig(1, 2, figsize=(16*cm, 7*cm))

    # read image file
    axes = subplots[0]
    data = load_minus_ends_data(dataset='full')
    plot_relative_position_distrib_kde(data, axes, groups=GROUPS,
                                       xlims=[-0.15, 0.93], grey_area=True)
    add_panel_label(axes, label='B')
    
    axes = subplots[1]
    counts = count_MT_per_fiber(data)
    plot_MTs_per_fiber(counts, axes, stripplot=False)
    add_panel_label(axes, label='C')
    
    savefig(fig, 'fig2')
        