from os.path import join

import numpy as np
import pandas as pd
import seaborn as sns

from kfibers.settings import (DATASETS, GROUPS, POSITION, TYPE1, TYPE2, GROUP,
                              CELL)
from kfibers.plots.plot_utils import (init_fig, savefig, PALETTE,
                                      create_patches_legend)
from kfibers.utils import load_minus_ends_data
from kfibers.plots.plot_utils import arrange_plot


def load_data(dataset, groupby=GROUP):
    bins = np.linspace(-0.3, 0.8, 11)
    data = load_minus_ends_data(dataset=dataset, only_matching=True)
    data['interval'] = pd.cut(data[POSITION], bins=bins)
    counts = data.groupby(['interval', groupby, TYPE1])[TYPE2].count().reset_index()
    c = pd.pivot_table(counts, index=['interval', groupby], values=TYPE2, columns=TYPE1)
    c['total'] = c.sum(1)
    c['% open'] = c[1] / c['total'] * 100
    c = c.reset_index()
    c['interval'] = ['[{:.2f}, {:.2f}]'.format(x.left, x.right) for x in c['interval']]
    return(c, bins)


def make_plot_groups(data, fname):
    fig, axes = init_fig(1, 1, colsize=3.5)
    sns.barplot(x='interval', y='% open', hue=GROUP,
                data=data, n_boot=1, ax=axes, palette=PALETTE)
    axes.plot((2, 2), (0, 100), lw=0.5, linestyle='--', c='grey')
    arrange_plot(axes, xlabel='Relative position', ylabel='% Open minus-ends',
                 ylims=(0, 100), despine=False)
    axes.set_xticklabels(axes.get_xticklabels(), rotation=45, fontsize=7,
                         ha='right')
    create_patches_legend(axes, PALETTE, loc=1, fontsize=7)
    savefig(fig, fname=fname)
    

def make_plot_single_group(data, fname, value, xorder, by=GROUP):
    d = data.loc[data[by] == value, :]
    d['i'] = [xorder.index(i) for i in d['interval']]
    fig, axes = init_fig(1, 1, colsize=3.5)
    
    sns.barplot(x='interval', y='total', order=xorder,
                data=d, n_boot=1, ax=axes, color=PALETTE.get(value, 'purple'))
    arrange_plot(axes, xlabel='Relative position', # ylims=ylims,
                 ylabel='Number of minus-ends',
                 despine=False, xlims=(-0.9, 10))
    axes.set_xticklabels(xorder, rotation=45, fontsize=7, ha='right')
    
    axes = axes.twinx()
    sns.lineplot(x='i', y='% open', data=d, ax=axes,
                 color=PALETTE.get(value, 'darkred'))
    arrange_plot(axes, xlabel='Relative position', ylabel='% Open minus-ends',
                 despine=False, xlims=(-0.9, 10), ylims=(0, 100))
    axes.plot((2, 2), (0, 100), lw=0.5, linestyle='--', c='grey')

    savefig(fig, fname=fname)


if __name__ == '__main__':
    # Plot comparison across groups for the different datasets
    for dataset in DATASETS:
        data, bins = load_data(dataset)
         
        make_plot_groups(data, fname='open_minus_ends.matching.{}'.format(dataset))
        xorder = ['[{:.2f}, {:.2f}]'.format(i, j) for i, j in zip(bins, bins[1:])]
        for group in GROUPS:
            fname = 'open_minus_ends.matching.{}.{}'.format(dataset, group)
            make_plot_single_group(data, fname, group, xorder=xorder)
    
    # Plot % by cell
    data, bins = load_data(dataset='full', groupby=CELL)
    xorder = ['[{:.2f}, {:.2f}]'.format(i, j) for i, j in zip(bins, bins[1:])]
    for cell in data[CELL].unique():
        fname = join('cells', 'open_minus_ends.matching.cell.{}'.format(cell))
        make_plot_single_group(data, fname, cell, xorder=xorder, by=CELL)
