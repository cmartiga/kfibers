import seaborn as sns

from kfibers.plots.plot_utils import (init_fig, savefig, arrange_plot,
                                      add_mean_sd_bars, PALETTE,
                                      create_patches_legend)
from kfibers.utils import read_inter_kinetocore_dist


if __name__ == '__main__':
    cm = 1/2.54
    inter_kinetocore_dist = read_inter_kinetocore_dist()
    
    fig, subplots = init_fig(1, 1, figsize=(16*cm, 8*cm))

    axes = subplots
    sns.stripplot(x='Cell', y='Interkinetochore distance (um)',
                  data=inter_kinetocore_dist, hue='Group', dodge=False,
                  palette=PALETTE, ax=axes, jitter=1,
                  linewidth=1, edgecolor='black')
    add_mean_sd_bars(axes, x='Cell', y='Interkinetochore distance (um)',
                     data=inter_kinetocore_dist)
    arrange_plot(axes, xlabel='Cell', showlegend=False, ylims=(0.4, 2),
                 ylabel=r'Outer-kinetochore distance ($\mu$m)')
    create_patches_legend(axes, PALETTE, loc=1, ncol=3)
    
    savefig(fig, 'figS3')
        