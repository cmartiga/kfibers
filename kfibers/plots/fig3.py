import numpy as np

from kfibers.settings import CONTROL, SIMCRS1, POSITION
from kfibers.plots.plot_utils import (init_fig, savefig,
                                      plot_percentages_posterior,
                                      arrange_plot,
                                      create_patches_legend, PALETTE,
                                      plot_params_posterior)
from kfibers.utils import (get_pred_X, load_model_fit, calc_open_perc_posterior,
                           calc_percentiles, load_minus_ends_data)


def get_data(interpolar=False):
    prefix = 'minus_ends'
    if interpolar:
        prefix = 'interpolar.' + prefix
        
    minus_ends = load_minus_ends_data(dataset=dataset, rm_undefined=False,
                                      only_matching=False, interpolar=interpolar)
    pred_X = get_pred_X(dataset, data=minus_ends, only_matching=False)
    traces = load_model_fit(dataset, only_matching=False, prefix=prefix)
    percentages = calc_open_perc_posterior(traces)
    
    # Merge coefficients
    traces = {'beta1': traces['beta2'] + traces['beta1']} 
        
    params_qs = calc_percentiles(traces)
    perc_qs = calc_percentiles(percentages)
    return(minus_ends, pred_X, perc_qs, params_qs)


if __name__ == '__main__':
    cm = 1/2.54
    xlims = (-0.15, 0.93)
    dataset = 'full'
    fig, subplots = init_fig(2, 2, figsize=(16*cm, 13*cm))
    subplots = subplots.flatten()
    
    # Plot kMTs
    axes = subplots[0]
    minus_ends, pred_X, perc_qs, params_qs = get_data(interpolar=False)
    params_qs = {k: v / np.log(2) for k, v in params_qs.items()}
    perc_qs = {k: v for k, v in perc_qs.items() if k != CONTROL}
    plot_percentages_posterior(pred_X, perc_qs, axes=axes, xlims=xlims)
    arrange_plot(axes, title='KMT minus-ends', xlabel='Relative distance')
    palette = {k: v for k, v in PALETTE.items() if k != CONTROL}
    create_patches_legend(axes, palette, loc=1, fontsize=8)
    axes.set_ylabel('Open minus-ends (%)')
    
    axes = subplots[1]
    ylims = (-3, 3)
    plot_params_posterior(pred_X, params_qs, axes=axes, xlims=xlims, 
                          xlabel='Relative distance', ylims=ylims, legend=False)
    axes.set_ylabel(r'$log_2$(open/closed)')
    axes.set_title('KMT minus-ends')
    
    # Plot non-kMTs
    axes = subplots[2]
    minus_ends, pred_X, perc_qs, params_qs = get_data(interpolar=True)
    params_qs = {k: v / np.log(2) for k, v in params_qs.items()}
    perc_qs = {k: v for k, v in perc_qs.items() if k != CONTROL}
    plot_percentages_posterior(pred_X, perc_qs, axes=axes, xlims=xlims,
                               linestyle='--')
    arrange_plot(axes, title='Non-KMT minus-ends', ylabel='Open minus-ends (%)',
                 xlabel=r'Relative distance')
    
    axes = subplots[3]
    plot_params_posterior(pred_X, params_qs, axes=axes, xlims=xlims,
                          ylims=ylims, xlabel=r'Relative distance',
                          linestyle='--')
    arrange_plot(axes, ylabel=r'$log_2$(open/closed)', showlegend=False, ylims=ylims,
                 title='Non-KMT minus-ends')
    
    # Savefig
    savefig(fig, fname='fig3')