// From https://mc-stan.org/users/documentation/case-studies/splines_in_stan.html

functions {
  vector build_b_spline(real[] t, real[] ext_knots, int ind, int order);
  vector build_b_spline(real[] t, real[] ext_knots, int ind, int order) {
    // INPUTS:
    //    t:          the points at which the b_spline is calculated
    //    ext_knots:  the set of extended knots
    //    ind:        the index of the b_spline
    //    order:      the order of the b-spline
    vector[size(t)] b_spline;
    vector[size(t)] w1 = rep_vector(0, size(t));
    vector[size(t)] w2 = rep_vector(0, size(t));
    if (order==1)
      for (i in 1:size(t)) // B-splines of order 1 are piece-wise constant
        b_spline[i] = (ext_knots[ind] <= t[i]) && (t[i] < ext_knots[ind+1]);
    else {
      if (ext_knots[ind] != ext_knots[ind+order-1])
        w1 = (to_vector(t) - rep_vector(ext_knots[ind], size(t))) /
             (ext_knots[ind+order-1] - ext_knots[ind]);
      if (ext_knots[ind+1] != ext_knots[ind+order])
        w2 = 1 - (to_vector(t) - rep_vector(ext_knots[ind+1], size(t))) /
                 (ext_knots[ind+order] - ext_knots[ind+1]);
      // Calculating the B-spline recursively as linear interpolation of two lower-order splines
      b_spline = w1 .* build_b_spline(t, ext_knots, ind, order-1) +
                 w2 .* build_b_spline(t, ext_knots, ind+1, order-1);
    }
    return b_spline;
  }
  
}data {
  int num_data;             // number of data points
  int num_groups;             // number of variables

  int num_knots;            // num of knots  
  vector[num_knots] knots;  // the sequence of knots
  int spline_degree;        // the degree of spline (is equal to order - 1)
  
  real X[num_data];
  int ends_number[num_data];
  matrix<lower=0, upper=1>[num_data, num_groups] design;
  
  int num_pred;
  real pred_X[num_pred];
}

transformed data {
  int num_basis = num_knots + spline_degree - 1; // total number of B-splines
  matrix[num_basis, num_data] B;  // matrix of B-splines
  vector[spline_degree + num_knots] ext_knots_temp;
  vector[2*spline_degree + num_knots] ext_knots; // set of extended knots
  
  int num_basis_pred = num_knots + spline_degree - 1; // total number of B-splines
  matrix[num_basis_pred, num_pred] B_pred;  // matrix of B-splines
  vector[spline_degree + num_knots] ext_knots_tmp_pred;
  vector[2*spline_degree + num_knots] ext_knots_pred; // set of extended knots
  
  
  ext_knots_temp = append_row(rep_vector(knots[1], spline_degree), knots);
  ext_knots = append_row(ext_knots_temp, rep_vector(knots[num_knots], spline_degree));
  for (ind in 1:num_basis)
    B[ind,:] = to_row_vector(build_b_spline(X, to_array_1d(ext_knots), ind, spline_degree + 1));
  B[num_knots + spline_degree - 1, num_data] = 1;
  
  ext_knots_tmp_pred = append_row(rep_vector(knots[1], spline_degree), knots);
  ext_knots_pred = append_row(ext_knots_tmp_pred, rep_vector(knots[num_knots], spline_degree));
  for (ind in 1:num_basis_pred)
    B_pred[ind,:] = to_row_vector(build_b_spline(pred_X, to_array_1d(ext_knots_pred), ind, spline_degree + 1));
  B_pred[num_knots + spline_degree - 1, num_pred] = 1;
}

parameters {
  row_vector[num_basis] a_raw;
  real a0;  // intercept
  real<lower=0> tau_a;
  
  matrix[num_basis, num_groups] b_raw;
  vector[num_groups] b0;  // intercept
  vector<lower=0> [num_groups] tau_b;
  
}

transformed parameters {
  row_vector[num_basis] a;          // spline coefficients
  matrix[num_basis, num_groups] b;  // spline coefficients
  
  vector[num_data] alpha;
  matrix[num_data, num_groups] beta;
  vector[num_data] means;
  
  a[1] = a_raw[1];
  for (i in 2:num_basis)
    a[i] = a[i-1] + a_raw[i]*tau_a;

  for (j in 1:num_groups){
    b[1, j] = b_raw[1, j];
    for (i in 2:num_basis)
      b[i, j] = b[i-1, j] + b_raw[i, j]*tau_b[j];
  }
  
  alpha = a0*to_vector(X) + to_vector(a*B);
  for(j in 1:num_groups)
    beta[,j] = b0[j]*to_vector(X) + to_vector(b[,j]'*B);
  
  means = alpha;
  for (j in 1:num_groups)
    means = means + beta[,j] .* design[,j];
}

model {
  // Priors
  a_raw ~ normal(0, 1);
  a0 ~ normal(0, 1);
  tau_a ~ normal(0, 1);
  
  to_row_vector(b_raw) ~ normal(0, 1);
  b0 ~ normal(0, 1);
  tau_b ~ normal(0, 1);

  //Likelihood
  ends_number ~ poisson_log(means);
}

generated quantities{
  vector[num_pred] pred_alpha;
  matrix[num_pred, num_groups] pred_beta;
    
  pred_alpha = a0*to_vector(pred_X) + to_vector(a*B_pred);
  
  for(j in 1:num_groups)
    pred_beta[,j] = b0[j]*to_vector(pred_X) + to_vector(b[,j]'*B_pred);
}

