data {
  int num_data;             // number of data points
  int num_obs;              // number of observers
  int num_cells;            // number of cells
  int num_groups;           // number of variables
  
  matrix <lower=0, upper=1>[num_data, num_cells] cells;
  matrix <lower=0, upper=1>[num_data, num_groups] design;
  
  int <lower=0, upper=1> open_ends[num_data, num_obs];
}

parameters {
  real alpha;
  vector[num_groups] beta;

  // random effects for cells
  vector[num_cells] cell_raw;
  real<lower=0> cell_sigma;
  
  // random effects for measurer
  vector[num_obs] measure_raw;
  real<lower=0> measure_sigma;
  
  // random effects microtubule
  vector[num_data] x_raw;
  real<lower=0> sigma;
}

transformed parameters {
  vector[num_data] means;
  vector[num_cells] cell_beta;
  vector[num_obs] measure_beta;
  
  cell_beta = cell_sigma * cell_raw;
  measure_beta = measure_sigma * measure_raw;
  
  means = alpha + design * beta + cells * cell_beta + sigma * x_raw; 
}

model {
  // Priors
  alpha ~ normal(0, 1.5);
  beta ~ normal(0, 1);
  
  cell_sigma ~ gamma(2, 1);
  measure_sigma ~ gamma(2, 1);
  sigma ~ gamma(2, 1);
  
  cell_raw ~ normal(0, 1);
  measure_raw ~ normal(0, 1);
  x_raw ~ normal(0, 1);
  
  //Likelihood
  for(m in 1:num_obs)
    open_ends[,m] ~ bernoulli_logit(means + measure_beta[m]);
}
