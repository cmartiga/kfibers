// Linear model open ~ 1 + fixed + (1 | cell) + (1 | Fiber) + (1 | microtubule)

data {
  int N;           // number of total observations
  int K;           // number of variables
  
  int C;           // number of cells
  int F;           // number of fibers
  int T;           // number of microtobules
  int M;           // number of different measurers

  int<lower=1, upper=C> cells[N];
  int<lower=1, upper=F> fibers[N];
  int<lower=1, upper=T> mts[N];
  int<lower=1, upper=M> measures[N];

  matrix[N, K] X;
  int<lower=0, upper=1> open[N];        // Bernuilli observation of open/closed
}

parameters {
  real alpha;        // intercept
  vector[K] beta;    // coefficients for fixed effects
  
  // Standard deviation of random effects
  real<lower=0> cells_sigma;
  //real<lower=0> fibers_sigma;
  real<lower=0> mts_sigma;
  real<lower=0> measure_sigma;
  
  vector[C] cells_raw;
  //vector[F] fibers_raw;
  vector[T] mts_raw;
  vector[M] measure_raw;
  
}

transformed parameters {
  vector[C] cells_beta;
  //vector[F] fibers_beta;
  vector[T] mts_beta;
  vector[M] measure_beta;
  
  vector[N] y;
  
  cells_beta = cells_sigma * cells_raw;
  //fibers_beta = fibers_sigma * fibers_raw;
  mts_beta = mts_sigma * mts_raw;
  measure_beta = measure_sigma * measure_raw;
  
  //  + fibers_beta[fibers]
  y = alpha + X * beta + cells_beta[cells] + mts_beta[mts] + measure_beta[measures];    
}

model {
  // Priors
  alpha ~ normal(0, 1);
  beta ~ normal(0, 1);

  cells_sigma ~ normal(0, 1);
  //fibers_sigma ~ normal(0, 1);
  mts_sigma ~ normal(0, 1);
  measure_sigma ~ normal(0, 1);
  
  cells_raw ~ normal(0, 1);
  //fibers_raw ~ normal(0, 1);
  mts_raw ~ normal(0, 1);
  measure_raw ~ normal(0, 1);
  
  //Likelihood
  open ~ bernoulli_logit(y);
}
