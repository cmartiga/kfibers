functions {
  vector gp_pred_rng(real[] x2,
                     vector y1, real[] x1,
                     real alpha, real rho, real sigma, real delta) {
    int N1 = rows(y1);
    int N2 = size(x2);
    vector[N2] f2;
    {
      matrix[N1, N1] K =   cov_exp_quad(x1, alpha, rho)
                         + diag_matrix(rep_vector(square(sigma), N1));
      matrix[N1, N1] L_K = cholesky_decompose(K);

      vector[N1] L_K_div_y1 = mdivide_left_tri_low(L_K, y1);
      vector[N1] K_div_y1 = mdivide_right_tri_low(L_K_div_y1', L_K)';
      matrix[N1, N2] k_x1_x2 = cov_exp_quad(x1, x2, alpha, rho);
      vector[N2] f2_mu = (k_x1_x2' * K_div_y1);
      matrix[N1, N2] v_pred = mdivide_left_tri_low(L_K, k_x1_x2);
      matrix[N2, N2] cov_f2 =   cov_exp_quad(x2, alpha, rho) - v_pred' * v_pred
                              + diag_matrix(rep_vector(delta, N2));
      f2 = multi_normal_rng(f2_mu, cov_f2);
    }
    return f2;
  }
}

data {
  int num_data;             // number of total
  int num_measures;             // number of total

  real X[num_data];
  
  int<lower=0> open_ends[num_data, num_measures];  // Bernuilli observation of open/closed
  vector<lower=0, upper=1>[num_data] group_b;
  
  int num_pred;
  real pred_X[num_pred];
}

parameters {
  real a_mean;
  real<lower=0> rho_a;
  real<lower=0> alpha_a;
  real<lower=0> sigma_a;
  vector[num_data] a_raw;
  
  real b_mean;
  real<lower=0> rho_b;
  real<lower=0> alpha_b;
  real<lower=0> sigma_b;
  vector[num_data] b_raw;
  
  matrix[2, num_data] x_raw;
  vector[num_measures] measure_raw;
  real<lower=0> measure_sigma;
}


transformed parameters {
  matrix[num_data, num_data] cov_a = cov_exp_quad(X, alpha_a, rho_a) + diag_matrix(rep_vector(square(1e-3), num_data)) ;
  matrix[num_data, num_data] L_cov_a = cholesky_decompose(cov_a);
  
  matrix[num_data, num_data] cov_b = cov_exp_quad(X, alpha_b, rho_b) + diag_matrix(rep_vector(square(1e-3), num_data));
  matrix[num_data, num_data] L_cov_b = cholesky_decompose(cov_b);
  
  vector[num_data] a = a_mean + L_cov_a * a_raw;
  vector[num_data] b = b_mean + L_cov_b * b_raw;
  
  vector[num_measures] measure_beta = measure_raw * measure_sigma;
}

model {
  a_mean ~ normal(0, 1);
  rho_a ~ inv_gamma(8.91924 / 20, 34.5805 / 20);
  alpha_a ~ normal(0, 2);
  sigma_a ~ normal(0, 1);
  a_raw ~ normal(0, 1);
  
  b_mean ~ normal(0, 1);
  rho_b ~ inv_gamma(8.91924 / 20, 34.5805 / 20);
  alpha_b ~ normal(0, 2);
  sigma_b ~ normal(0, 1);
  b_raw ~ normal(0, 1);
  
  //Likelihood
  for(c in 1:num_measures)
    open_ends[,c] ~ bernoulli_logit(a + b .* group_b + measure_beta[c]);
}

generated quantities {
  vector[num_pred] pred_alpha = a_mean + gp_pred_rng(pred_X, a, X, alpha_a, rho_a, sigma_a, 1e-10);
  vector[num_pred] pred_beta = b_mean + gp_pred_rng(pred_X, b, X, alpha_b, rho_b, sigma_b, 1e-10);
}
