data {
  int num_data;             // number of data points
  int num_obs;              // number of observers
  int num_cells;              // number of cells
  int num_groups;             // number of variables
  int num_cat;             // number of categories
  
  matrix <lower=0, upper=1>[num_data, num_cells] cells;
  matrix <lower=0, upper=1>[num_data, num_groups] design;
  
  int <lower=1, upper=num_cat> open_ends[num_data, num_obs];
  
}

parameters {
  vector[num_cat] alpha;
  matrix[num_groups, num_cat] beta;

  // random effects for cells
  matrix[num_cells, num_cat] cell_raw;
  real<lower=0> cell_sigma;
  
  // random effects for measurer
  matrix[num_cat, num_obs] measure_raw;
  real<lower=0> measure_sigma;
}

transformed parameters {
  matrix[num_cat, num_data] means;
  matrix[num_cells, num_cat] cell_beta;
  matrix[num_cat, num_obs] measure_beta;
  
  cell_beta = cell_sigma * cell_raw;
  measure_beta = measure_sigma * measure_raw;
  
  means = rep_matrix(alpha, num_data) + (design * beta)' + (cells * cell_beta)'; 
}

model {
  // Priors
  alpha ~ normal(0, 1.5);
  to_row_vector(beta) ~ normal(0, 1);
  
  cell_sigma ~ gamma(2, 1);
  measure_sigma ~ gamma(2, 1);
  
  to_row_vector(cell_raw) ~ normal(0, 1);
  to_row_vector(measure_raw) ~ normal(0, 1);
  
  //Likelihood
  for(i in 1:num_data)
    for(m in 1:num_obs)
      open_ends[i,m] ~ categorical_logit(means[,i] + measure_beta[,m]);
}
