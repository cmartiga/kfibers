
data {
    int <lower=0> N;        // number of microtubules
    int <lower=0> K;        // number of A concentrations
    
    matrix <lower=0; upper=1> [N, K] design;
    
    vector[K] A;
    
    vector[N] ab;
    vector[N] ac;
    
    


}


parameters {
    real log_K1;
    real logfc;
    
    real log_B;              // Total B concentration
    real log_C;              // Total C concentration
    
    vector[N] log_B_raw;
    vector[N] log_C_raw;
    
    real<lower=0> sigma;     // Variability due to local perturbations
    real<lower=0> tau;       // Measurement error

}

transformed parameters {
    real K1;
    real K2;
    
    K1 = exp(log_K1);
    K2 = exp(log_K2 + logfc);
}

model {
  // Priors
  
  //Likelihood
  
}

