#!/usr/bin/env python

from setuptools import setup, find_packages
from kfibers.settings import VERSION


def main():
    description = 'Analysis of minus-ends tomography and in vitro binding'
    setup(
        name='kfiber',
        version=VERSION,
        description=description,
        author_email='carlos199m1@gmail.com',
        url='https://bitbucket.org/cmartiga/kfibers',
        packages=find_packages(),
        include_package_data=True,
        entry_points={
            'console_scripts': []},
        install_requires=['numpy', 'pandas', 'scipy', 'seaborn', 'matplotlib',
                          'pystan', 'statsmodels'],
        platforms='ALL',
        keywords=['microtubules', 'minus-ends'],
        classifiers=[
            "Programming Language :: Python :: 3",
            'Intended Audience :: Science/Research',
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
        ],
    )
    return


if __name__ == '__main__':
    main()
